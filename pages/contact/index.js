import {IoIosMail} from 'react-icons/io'
import {MdLocalPhone} from 'react-icons/md'
import {GrLinkedin} from 'react-icons/gr'

import '../../styles/contact.scss'

export default() => {
    return (
        <div className="columns contact mb-6">
            <div className="column is-8 is-offset-2 contactFrame mb-6">
                <h1 className="title  has-text-centered mt-2">Me contacter</h1>
                <h2 className="subtitle  has-text-centered mt-2">Pour toutes questions, n'hésitez pas à me contacter</h2>
                <div className="columns mt-6">
                    <div className="column is-3 is-offset-3">
                        <div className="roundFrame">
                            <IoIosMail size={80} className="iconContact"/>
                        </div>
                        <p className="mt-2 is-uppercase has-text-centered">Mail</p>
                        <a href={`mailto:${process.env.mailPro}`}>
                        <p className="mt-5">{process.env.mailPro}</p>

                        </a>
                    </div>
                    <div className="column is-3 is-offset-1 has-text-centered">
                        <div className="roundFrame">
                            <MdLocalPhone size={80} className="iconContact"/>
                        </div>
                        <p className="mt-2 is-uppercase">Phone</p>
                        <p className="mt-5">{process.env.telephoneMobile}</p>
                    </div>
                </div>
                <div className="columns mt-6">
                    <div className="column is-3 is-offset-3 has-text-centered">
                        <div className="roundFrame">
                            <GrLinkedin size={80} className="iconContact"/>
                        </div>
                        <p className="mt-2 is-uppercase">Linkedin</p>
                        <a href={process.env.linkedinWebPage}>
                            <p className="mt-5">{process.env.nameLastName}</p>
                        </a>
                    </div>
                    {/* <div className="column is-3 is-offset-1 has-text-centered">
                        <div className="roundFrame">
                            <p className="has-text-white is-size-1 has-text-weight-bold	malt">malt</p>
                        </div>
                        <p className="mt-2 is-uppercase">Malt</p>
                        <a href={process.env.maltWebPage}>
                            <p className="mt-5">Site web</p>
                        </a>
                    </div> */}
                </div>
            </div>
        </div>
    )
}