import {useRouter} from 'next/router'

import ArticlePreviewList from '../../components/ArticlePreviewList'

export default({articlesJson}) => {
    const router = useRouter()
    const TagFilter = router.query.guide
    return (
    <>
            <h1 className="title has-text-centered">Liste des articles <span className="is-capitalized	">{TagFilter}</span></h1>
                <ArticlePreviewList
                    articlesJson={articlesJson}
                    options={{
                    search: TagFilter
                }}/>
        </>
    )
}