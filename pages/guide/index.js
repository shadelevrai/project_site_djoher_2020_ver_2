import ArticlePreviewList from "../../components/ArticlePreviewList"

export default({articlesJson}) => {
    return (
        <div className="columns">
            <div className="column">
                <h1 className="title has-text-centered">La liste des articles</h1>
                <ArticlePreviewList articlesJson={articlesJson}/>
            </div>
        </div>
    )
}