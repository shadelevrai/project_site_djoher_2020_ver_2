import '../../styles/link.scss'

export default() => {
    const datas = [
        {
            name: "Javascript",
            infos: [{
                description: "Le MDN de javascript ",
                link: "https://developer.mozilla.org/fr/docs/Web/JavaScript"
            },{
                description: "Apprendre Javascript avec open class room",
                link: "https://openclassrooms.com/fr/courses/1916641-dynamisez-vos-sites-web-avec-javascript"
            },{
                description: "Une liste des fonctions Javascript avec des exemples",
                link: "https://www.w3schools.com/js/"
            },{
                description: "Apprendre Javascript en tapant directement du code",
                link: "https://www.codecademy.com/learn/introduction-to-javascript"
            },{
                description: "La plus grosse communauté Javascript",
                link: "https://www.reddit.com/r/javascript/"
            },{
                description: "Le plus gros groupe facebook de javascript",
                link: "https://www.facebook.com/groups/alljavascript"
            }]
        },{
            name:"Jquery",
            infos:[{
                description: "Le site officiel de Jquery",
                link: "https://openclassrooms.com/fr/courses/1631636-simplifiez-vos-developpements-javascript-avec-jquery"
            },{
                description:"Apprendre Jquery avec open class room",
                link:"https://openclassrooms.com/fr/courses/1631636-simplifiez-vos-developpements-javascript-avec-jquery"
            },{
                description:"Le site oficiel pour Jquery UI",
                link:"https://jqueryui.com/"
            }]
        },{
            name:"React",
            infos:[{
                description: "Le site officiel de React",
                link: "https://fr.reactjs.org/"
            },{
                description:"Apprendre React avec open class room",
                link:"https://openclassrooms.com/fr/courses/4664381-realisez-une-application-web-avec-react-js/4664388-decouvrez-lutilite-et-les-concepts-cles-de-react"
            },{
                description:"Quelques explications très simple de React",
                link:"https://www.w3schools.com/react/"
            },{
                description:"Les hooks de React",
                link:"https://react-hook-form.com/get-started"
            },{
                description:"React pour mobile",
                link:"https://reactnative.dev/"
            }]
        },{
            name:"Vue",
            infos:[{
                description: "Le site officiel de Vue",
                link: "https://vuejs.org/"
            },{
                description:"Les bases de Vue en vidéo",
                link:"https://www.youtube.com/watch?v=D3oivlcoEvw"
            },{
                description:"Vue pour les débutants sur medium ",
                link:"https://medium.com/@thibault60000/vue-js-pour-les-d%C3%A9butants-37a83aaea9c4"
            }]
        },{
            name:"Mongo",
            infos:[{
                description: "Le site officiel de Mongo",
                link: "https://www.mongodb.com/fr"
            },{
                description:"Apprendre Mongo avec open clas room",
                link:"https://openclassrooms.com/fr/courses/4462426-maitrisez-les-bases-de-donnees-nosql/4474601-decouvrez-le-fonctionnement-de-mongodb"
            },{
                description:"Avoir une base de donnée Mongo en ligne gratuitement",
                link:"https://mlab.com/"
            }]
        },{
            name:"Node",
            infos:[{
                description: "Le site officiel de Node",
                link: "https://nodejs.org/en/"
            },{
                description:"Apprendre Node et Express avec open class room",
                link:"https://openclassrooms.com/fr/courses/1056721-des-applications-ultra-rapides-avec-node-js/1056866-node-js-mais-a-quoi-ca-sert"
            },{
                description:"Site pour Expres",
                link:"https://expressjs.com/fr/"
            }]
        },{
            name:"Angular",
            infos:[{
                description: "Le site officiel de Angular",
                link: " https://angular.io/"
            },{
                description:"Apprendre Angular avec oen class room",
                link:"https://openclassrooms.com/fr/courses/4668271-developpez-des-applications-web-avec-angular"
            }]
        },{
            name:"HTML",
            infos:[{
                description: "Le HTML avec w3school",
                link: "https://www.w3schools.com/html/"
            },{
                description:"Le MDN mozilla du HTML",
                link:"https://developer.mozilla.org/fr/docs/Web/HTML"
            },{
                description:"Apprendre HTML avec open class room",
                link:"https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3/1604361-creez-votre-premiere-page-web-en-html"
            },{
                description:"Explication de l'imprtance de la sémantique en HTML",
                link:"https://www.freecodecamp.org/news/semantic-html5-elements/"
            }]
        },{
            name:"Git",
            infos:[{
                description: "Apprendre Git en live",
                link: "https://learngitbranching.js.org/?locale=fr_FR"
            },{
                description:"Le site officiel de Git",
                link:"https://git-scm.com/"
            },{
                description:"Hébergeur gratuit",
                link:"https://about.gitlab.com/"
            },{
                description:"Les lignes Git les plus simples",
                link:"https://rogerdudler.github.io/git-guide/index.fr.html"
            },{
                description:"Gitkraken, un utilitaire graphique très pratique",
                link:"https://www.gitkraken.com/"
            }]
        }
    ]
    return (
        <div className="linkFrame mb-6">
            <h1 className="title has-text-centered">Les liens utiles pour Javascript</h1>
            <h2 className="subtitle has-text-centered">Voici quelques sites qui vont pouvoir vous aider à parfaire votre maîtrise en Javascript</h2>
                <div className="columns is-mobile">
                    <div className="column is-8 is-offset-2 linkDescription mt-5 pb-6 is-2-offset-mobile is-8-mobile">
                        {datas.map(data=>
                        <div key={data.name}>
                            <p className="is-size-3	has-text-white has-text-centered mt-5">{data.name}</p>
                            <ul className="mt-4">
                                {data.infos.map(info=>
                                    <li key={info.link}>
                                        <a className="has-text-white ml-6" href={info.link}>{info.description}</a>
                                    </li>
                                )}
                            </ul>
                        </div>    
                        )}
                    </div>
                </div>
        </div>
    )
}