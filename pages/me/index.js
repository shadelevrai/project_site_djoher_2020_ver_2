import "../../styles/me.scss";
import { FaGuitar, FaPlane, FaLinkedin } from "react-icons/fa";
import { IoMdMicrophone, IoLogoGameControllerB } from "react-icons/io";

export default () => {
  const experiences = [
    /*  {
            post: "Développeur Web",
            type: "Temps plein",
            entreprise: "Aliznet",
            city: "Paris",
            dateBegin: "02/2020",
            dateEnd: "06/2020",
            langages: ["NextJS"]
        }, {
            post: "Développeur Web",
            type: "Temps plein",
            entreprise: "Zenika",
            city: "Paris",
            dateBegin: "10/2019",
            dateEnd: "01/2020",
            langages: ["Javascript", "React", "Node", "Mongo"]
        }, {
            post: "Développeur Web",
            type: "Temps plein",
            entreprise: "MM5 Production",
            city: "Paris",
            dateBegin: "06/2019",
            dateEnd: "09/2020",
            langages: ["React", "Node", "Mongo"]
        }, {
            post: "Développeur Web",
            type: "Temps plein",
            entreprise: "Djoher entreprise",
            city: "Paris",
            dateBegin: "02/2019",
            dateEnd: "09/2019",
            langages: ["Jquery", "Node", "Mongo"]
        }, {
            post: "Développeur Web",
            type: "Temps plein",
            entreprise: "Salary tech",
            city: "Paris",
            dateBegin: "07/2019",
            dateEnd: "08/2019",
            langages: ["Angular", "Node", "Mongo"]
        }, {
            post: "Développeur Web",
            type: "Temps plein",
            entreprise: "Itnovem",
            city: "Paris",
            dateBegin: "08/2018",
            dateEnd: "04/2019",
            langages: ["Angular", "Node"]
        }, {
            post: "Développeur Web",
            type: "Temps plein",
            entreprise: "Gaijin Team Association",
            city: "Paris",
            dateBegin: "01/2018",
            dateEnd: "07/2019",
            langages: ["Jquery", "Node", "Mongo"]
        }, {
            post: "Développeur Web",
            type: "Temps plein",
            entreprise: "Djoher entreprise",
            city: "Paris",
            dateBegin: "09/2017",
            dateEnd: "12/2017",
            langages: ["Angular", "Node", "Mongo"]
        }, {
            post: "Développeur Web",
            type: "Temps plein",
            entreprise: "Altran",
            city: "Paris",
            dateBegin: "09/2016",
            dateEnd: "08/2017",
            langages: ["Jquery", "Node", "Mongo"]
        }, */ {
      post: "Développeur web en formation",
      type: "Temps plein",
      entreprise: "Ifocop",
      city: "Paris",
      dateBegin: "09/2020",
      dateEnd: "05/2021",
      langages: ["Javascript", "Node", "Mongo", "Angular", "React"],
    },
    {
      post: "Rédacteur en chef",
      type: "Temps plein",
      entreprise: "Otidem",
      city: "Paris",
      dateBegin: "09/2008",
      dateEnd: "07/2020",
      langages: [],
    },
    {
      post: "Maquettiste",
      type: "Temps plein",
      entreprise: "Otidem",
      city: "Paris",
      dateBegin: "02/2008",
      dateEnd: "09/2008",
      langages: [],
    },
  ];

  const presentationTxT = "Ami de la technologie et du progrès, je suis développeur web depuis peu. Avant d’entrer dans cet univers, j’étais rédacteur en chef d’un magazine papier pendant plusieurs années. Cette expérience a été très bénéfique pour le côté organisation et management. Maintenant, je suis développeur spécialisé dans le langage Javascript. Je peux donc faire du front (React, Angular) comme du back (Node, Mongo, Meteor…). J’ai eu la chance de participer à de nombreuses petits projets, ce qui m’a fait travailler avec plusieurs développeurs dont j’ai pu acquérir à chaque fois un morceau de leur savoir. Et quand j’ai du temps libre, je fais des tutoriels afin d’aider les plus junior à mieux comprendre ce monde qu’est Javascript.";
  return (
    <div className="me mb-6">
      <div className="languageFrame is-hidden-mobile">
        <div className="columns">
          <div className="column is-1 is-offset-2 is-uppercase is-size-4 has-text-weight-bold has-text-white">javascript</div>
          <div className="column is-1 is-offset-1 is-uppercase is-size-5 has-text-weight-bold has-text-white">vue</div>
          <div className="column is-1 is-offset-1 is-uppercase is-size-4 has-text-weight-bold has-text-white">react</div>
          <div className="column is-1 is-offset-1 is-uppercase is-size-5 has-text-weight-bold has-text-white">mongo</div>
        </div>
        <div className="columns">
          <div className="column is-1 is-offset-3 is-uppercase is-size-5 has-text-weight-bold has-text-white">angular</div>
          <div className="column is-1 is-offset-1 is-uppercase is-size-4 has-text-weight-bold has-text-white">html</div>
          <div className="column is-1 is-offset-1 is-uppercase is-size-4 has-text-weight-bold has-text-white">nextjs</div>
          <div className="column is-1 is-offset-1 is-uppercase is-size-6 has-text-weight-bold has-text-white">css</div>
        </div>
        <div className="columns">
          <div className="column is-1 is-offset-2 is-uppercase is-size-6 has-text-weight-bold has-text-white">meteor</div>
          <div className="column is-1 is-offset-1 is-uppercase is-size-4 has-text-weight-bold has-text-white">node</div>
          <div className="column is-1 is-offset-1 is-uppercase is-size-5 has-text-weight-bold has-text-white">jquery</div>
          <div className="column is-1 is-offset-1 is-uppercase is-size-6 has-text-weight-bold has-text-white">ajax</div>
        </div>
      </div>
      <h1 className="title has-text-centered is-size-1">{process.env.nameLastName}</h1>
      <h2 className="subtitle has-text-centered">Développeur web Javascript</h2>
      <div className="has-text-centered mt-4">
        <a href={process.env.linkedinWebPage}>
          <FaLinkedin className="is-large icon has-text-dark" />
        </a>
        {/* <button className="button is-normal buttonDownload">Télécharger mon dossier de compétence</button> */}
      </div>
      <div className="columns">
        <div className="column is-5 is-offset-1 is-10-mobile is-offset-1-mobile">
          <figure className="image">
            <img className="is-rounded imageMe" src="/seo.png" alt="" />
          </figure>
          <div className="textPresentation">
            <h2 className="has-text-centered presentation subtitle has-text-weight-bold is-size-4 has-text-white">Présentation</h2>
            <p className="has-text-justified has-text-white">{presentationTxT}</p>
          </div>
          <h2 className="has-text-centered passionTitle subtitle has-text-weight-bold is-size-4">Mes passions</h2>
          <p className="is-size-5">
            <FaPlane /> Passer toutes mes vacances au Japon
          </p>
          <p className="is-size-5">
            <FaGuitar /> Jouer de la guitare et me faire mal aux doigts
          </p>
          <p className="is-size-5">
            <IoMdMicrophone /> Chanter pour que ma guitare se sente moins seule
          </p>
          <p className="is-size-5">
            <IoLogoGameControllerB /> Les jeux vidéo pour vivre de grandes aventures
          </p>
        </div>
        <div className="column is-5 is-offset-1 is-8-mobile is-offset-2-mobile">
          <h2 className="subtitle has-text-centered has-text-weight-bold is-size-4 mission">Mon expérience</h2>
          <div className="columns verticalLigne">
            <div className="column is-11 is-offset-1 ">
              {experiences.map((experience) => (
                <ul key={experience.entreprise}>
                  <li className="has-text-success has-text-weight-bold">{experience.entreprise}</li>
                  <li className="has-text-weight-bold">{experience.post}</li>
                  <li>
                    {experience.city} du {experience.dateBegin} au {experience.dateEnd}
                  </li>
                  {experience.langages.map((langage) => (
                    <span key={langage} className="tag is-success is-light">
                      {" "}
                      {langage}{" "}
                    </span>
                  ))}
                  <hr />
                </ul>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
