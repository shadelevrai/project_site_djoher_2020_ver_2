import decode from 'unescape'
import Head from 'next/head'

import articlesJson from '../../data/articlesJson'
import Tags from '../../components/micro/tags'
import Signature from '../../components/signature'

import '../../styles/article.scss'

const Article = ({data}) => {
    const {title, description, langage, intro, createDate, readminutes, article} = data

    function displayElement({elementCategory, value}) {
        if (elementCategory === 'subtitle') {
            return <p className='subtitle is-3 has-text-weight-bold'>{value}</p>
        }
        if (elementCategory === 'text') {
            return <p className="text is-1 has-text-weight-semibold">{value}</p>
        }
        if (elementCategory === 'code') {
            return <pre className="code has-text-weight-semibold">{decode(value)}</pre>
        }
        if (elementCategory === 'img') {
            return <figure className="columns is-centered">
                {/* eslint-disable-next-line no-undef */}
                <img
                    className="image imgArticle"
                    // eslint-disable-next-line no-undef
                    src={`/article/${title}/${value}.png`} alt="image pour habiller article"></img>
            </figure>
        }
        if (elementCategory === 'redTitle') {
            return <h2 className='subtitle is-3 has-text-danger has-text-weight-bold'>{value}</h2>
        }
        if (elementCategory === 'list') {
            return <ol
                dangerouslySetInnerHTML={{
                __html: decode(value)
            }}></ol>
        }
        if (elementCategory === "link") {
            return <a href={value}>{value}</a>
        }
        if (elementCategory === "html") {
            return <div dangerouslySetInnerHTML={{__html:decode(value)}}></div>
        }
    }

    return (
        <>
         <Head>
      <title>{title}</title>
      <meta name="description" content={description}/>
      <meta property="og:image" content={`/imgintro/${title}.png`} />
    <meta property="og:image:width" content="180"/>
    <meta property="og:image:height” content=”110" />
    <meta property='og:title' content={title}/>
    <meta property='og:description' content={intro}/>
    <meta name='author' content='Ryan Djoher, Ryan@djoher-dev.com'/>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link rel="shortcut icon" href="/favicon.png" />
    </Head>
    
        <div className="article mb-6">
            <h1 className="title has-text-centered is-size-2">{title}</h1>
            <div className="columns is-centered tags">
                <Tags tags={langage} size={"medium"}/>
            </div>
            <figure className="columns is-centered">
                <img // eslint-disable-next-line no-undef
                    src={`/imgintro/${title}.png`} className="image imgIntro" alt={title}></img>
            </figure>
            <div className="columns">
                <div className="column is-8 is-offset-2">
                    <h2 className="is-size-5 has-text-centered has-text-weight-bold intro">{intro}</h2>
                    <div className="columns has-background-light signatureSpace">
                        <div className="column is-10 is-offset-1">

                            <Signature
                                articleDateCreate={createDate}
                                articleReadminutes={readminutes}/>
                        </div>
                    </div>
                    {article
                        .map((item,index) => <div key={index}> {displayElement(item) }</div>)}
                </div>
            </div>
        </div> 
        </>
    )
}

export async function getServerSideProps({query}) {
    const titleLink = query.article

    const data = articlesJson.filter(article=>article.titleLink === titleLink)
    console.log("getServerSideProps -> data", data)

    return {
        props: {
            data : data[0]
        }
    }
}

export default Article