import Head from 'next/head'

import ArticlePreviewList from '../components/ArticlePreviewList'
import TwitterWhoToFollow from '../components/TwitterWhoToFollow'

import '../styles/index.scss'

export default({langage,articlesJson}) => {

    return (
        <>
            <Head>
                <title>Bienvenue sur Djoher-dev</title>
                <meta property="og:image" content={`/imgShareHome.png`} />
                <meta property="og:image:width" content="180" />
                <meta property="og:image:height” content=”110" />
                <meta property='og:title' content={"Djoher-dev, le site qui va peut-être vous apprendre quelque chose"} />
                <meta property='og:description' content={"Bienvenue sur Djoher-dev.com, le site de truc et astuce sur tout l'univers de Javascript. Retrouvez de nombreux articles qui vous aideront dans la création de site web et d'applications"} />
                <meta name='author' content='Ryan Djoher, Ryan@djoher-dev.com' />
                <meta name="description"
                    content="Bienvenue sur Djoher-dev.com, le site de truc et astuce sur tout l'univers de Javascript. Retrouvez de nombreux articles qui vous aideront dans la création de site web et d'applications" />
                <meta charSet="utf-8" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <link rel="shortcut icon" href="/favicon.png" />
            </Head>
            <div className="index">
                <p>Site en pause</p>
                {/* <section>
                    <div className="columns">
                        <div className="column is-8 is-offset-2">
                            <div className="columns thebluediv">
                                <div className="column is-2 ml-5 mt-5">
                                    <a href="/me">
                                        <figure className='image'>
                                            <img src="/seo.png" alt="" className="is-rounded" />
                                        </figure>
                                        <p className="has-text-centered has-text-grey">Ryan Djoher</p>
                                    </a>
                                </div>
                                <div className="column is-9 mb-2 mt-2">
                                    <h1 className='title has-text-white is-size-4'>Astuces et tutoriels Javascript</h1>
                                    <h2 className="subtitle has-text-white is-size-5">Bienvenue sur Djoher-dev, le
                                        site qui va peut-être vous apprendre quelque chose</h2>
                                    {langage
                                    .data
                                    .map(({name,link}) => <div key={name}
                                        className='button is-success buttonLangage mt-1'>
                                        <a href={`guide/${link}`} className="has-text-weight-bold">{name}</a>
                                    </div>)}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <p className="is-size-6 mt-6 has-text-centered has-text-weight-semibold">Pour être à l'aise dans la
                    lecture
                    des articles, je vous conseille d'avoir des bases dans le HTML/CSS et Javascript</p>
                <div className="columns">
                    <div className="column is-4 is-offset-4">
                        <hr />
                    </div>
                </div>
                <div className="columns">
                    <div className="column is-9 is-12-touch">
                        <ArticlePreviewList articlesJson={articlesJson} />
                    </div>
                    <div className="column is-3 is-hidden-touch">
                        <TwitterWhoToFollow />
                    </div>
                </div> */}
            </div>
        </>
    )
}