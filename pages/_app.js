import '../styles/import-bulma.sass'
import Layout from '../components/Layout'
import langage from "../data/langage"
import articlesJson from "../data/articlesJson"

export default function MyApp({Component, pageProps}) {

    return (
        <Layout>
            <Component {...pageProps}></Component>
        </Layout>
    )
}
MyApp.getInitialProps = async() => {
  
    return { pageProps : {
        langage,
        articlesJson
    }}
}
