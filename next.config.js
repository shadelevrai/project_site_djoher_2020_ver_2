const withSass = require('@zeit/next-sass')
module.exports = withSass({
    /* config options here */
    env:{
      nameWebSite:'Djoher-dev.com',
      nameLastName: 'Ryan Djoher',
      mailPro:'Ryan@djoher-dev.com',
      HOSTfront : 'http://89.2.199.106:8880',
      telephoneMobile:'+33 7 84 67 80 54',
      linkedinWebPage:'https://www.linkedin.com/in/ryan-djoher-b51388b0/',
      maltWebPage:'https://www.malt.fr/profile/khaleddjoher?q=khaled%20djoher&searchid=5f0716bef22b93000784b9e6'
    }
})