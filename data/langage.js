const langage = {
  data : [{
    "_id": {
        "$oid": "5e8c87aae7179a7912b7b279"
    },
    "name": "Node",
    "link": "node"
  },{
    "_id": {
        "$oid": "5e8c87c8e7179a7912b7b28a"
    },
    "name": "Mongo",
    "link": "mongo"
  },{
    "_id": {
        "$oid": "5e8c87e2e7179a7912b7b28d"
    },
    "name": "Socket.io",
    "link": "socketio"
  },{
    "_id": {
        "$oid": "5e8c8803e7179a7912b7b297"
    },
    "name": "React",
    "link": "react"
  },{
    "_id": {
        "$oid": "5e8c8823e7179a7912b7b2aa"
    },
    "name": "Angular",
    "link": "angular"
  },{
    "_id": {
        "$oid": "5e8c8831e7179a7912b7b2af"
    },
    "name": "Vue",
    "link": "vue"
  },{
    "_id": {
        "$oid": "5e8c8843e7179a7912b7b2b3"
    },
    "name": "Git",
    "link": "git"
  },{
    "_id": {
        "$oid": "5f29bf34e7179a1d1c3dbab0"
    },
    "name": "Javascript",
    "link": "javascript"
  }]
}

export default langage