const articlesJson = [{
    "_id": {
        "$oid": "5f236078b389f41b6ccd248a"
    },
    "langage": [
        "react"
    ],
    "category": "Astuce",
    "description": "Comment éviter de trop utiliser inutilement le State pour de pas perdre en performance.",
    "title": "Ne pas abuser du state",
    "titleLink": "ne-pas-abuser-du-state",
    "intro": "Quand j'ai appris React en parcourant mille tuto sur le net, je suis tombé sur pas mal d'exemple d'utilisation du state. Loin de moi l'envie de les critiquer, ils m'ont beaucoup appris, mais quand je revois les notes que j'avais noté à l'époque, je me rends compte qu'il y a certains problèmes quant à l'utilisation du State. Dans ce petit tuto, je vais vous expliquer pourquoi il ne faut pas en abuser, et comment bien l'utiliser. ",
    "article": [{
            "elementCategory": "subtitle",
            "value": "State by me"
        },
        {
            "elementCategory": "text",
            "value": "Le state, c'est un peu la mémoire d'un script javascript. Si vous ne savez pas ce qu'est un state, c'est que vous n'avez pas encore appris React. Mais je parie ma Playstation 4 que si vous êtes là, c'est que vous connaissez déjà React."
        },
        {
            "elementCategory": "text",
            "value": "Les cas que l'on voit beaucoup dans les tutoriels, c'est les formulaires. Et c'est normal car c'est une façon très pratique pour l'utilisation d'un State. Du coup, je vais vous faire un petit code formulaire sans prétention afin que vous compreniez pourquoi il c'est important de bien utiliser le State."
        },
        {
            "elementCategory": "code",
            "value": "import React, { useState } from 'react';\n\nfunction App() {\n\n  const [lastname, setLastname] = useState(null)\n  const [firstname, setFirstname] = useState(null)\n  const [city, setCity] = useState(null)\n\n  const changeLastName = e => {\n    setLastname(e.target.value)\n  }\n\n  const changeFirstName = e => {\n    setFirstname(e.target.value)\n  }\n\n  const changeCity = e => {\n    setCity(e.target.value)\n  }\n\n  const confirm = e => {\n    e.preventDefault()\n    console.log(`On envoit ${lastname}, ${firstname} et ${city}`)\n  }\n\n  return (\n    &lt;div>\n      &lt;h1>Mon petit formulaire à moi&lt;/h1>\n      &lt;form onSubmit={confirm}>\n        &lt;label htmlFor=\"lastname\">Nom :&lt;/label> \n        &lt;input type=\"text\" id=\"lastname\" onChange={changeLastName}/>\n        &lt;label htmlFor=\"firstname\">Prénom :&lt;/label> \n        &lt;input type=\"text\" id=\"firstname\" onChange={changeFirstName}/>\n        &lt;label htmlFor=\"city\">Ville :&lt;/label> \n        &lt;input type=\"text\" id=\"city\" onChange={changeCity}/>\n        &lt;button type=\"submit\">Valider&lt;/button>\n      &lt;/form>\n    &lt;/div>\n  );\n}\n\nexport default App;\n"
        },
        {
            "elementCategory": "text",
            "value": "Ici, on entre son nom, son prénom et sa ville puis on valide afin d'envoyer ces infos là où on souhaite. Ce genre précis de cas de figure, on le voit beaucoup dans mes exemples. En entrant des lettres dans l'input du nom, la fonction \"changeLastName\" qui est dans le \"onchange\" se lance et envoi directement les lettres dans cette fonction qui lui, utilisera le \"setLastname\" pour mettre la variable \"lastName\" à jour."
        },
        {
            "elementCategory": "redTitle",
            "value": "Ne faites jamais ça !"
        },
        {
            "elementCategory": "text",
            "value": "N'oubliez pas une chose : à chaque fois que le state se met à jour, il y a un re-render de votre page. Le dom virtuel est mis à jour, il est compré au vrai dom... bref, vous savez déjà tout ça."
        },
        {
            "elementCategory": "text",
            "value": "Un re-render est utile si on veut mettre à jour des informations qui sont affichés sur la page (ou pour plein d'autres choses mais ce n'est pas le sujet). Or dans ce cas de figure, l'affichage de notre page ne change pas quand on entre notre nom. Ce n'est pas comme si dans le titre, on voulait que le nom soit mis à jour en même temps que la personne la tape sur le input. Ce qu'on veut, c'est stocké en temps réelle le nom quelque part pour qu'on puisse le récupérer lorsque l'utilisateur validera le formulaire. "
        },
        {
            "elementCategory": "text",
            "value": "C'est donc ici une mauvaise pratique du state, car on fait un re-render inutile à chaque fois qu'on tape une lettre dans les input. Et en termes de performance, c'est pas top."
        },
        {
            "elementCategory": "subtitle",
            "value": "La solution, c'est sans state."
        },
        {
            "elementCategory": "text",
            "value": "C'est bien beau de vous dire qu'il ne faut pas utiliser le state de cette manière, mais si on veut faire exactement pareille dans l'esprit, comment on fait sans le state ? Facile :"
        },
        {
            "elementCategory": "code",
            "value": "import React from 'react';\n\nfunction App() {\n\n  let lastname\n  let firstname\n  let city\n\n  const changeLastName = e => {\n    lastname = e.target.value\n  }\n\n  const changeFirstName = e => {\n    firstname = e.target.value\n  }\n\n  const changeCity = e => {\n    city = e.target.value\n  }\n\n  const confirm = e => {\n    e.preventDefault()\n    console.log(`On envoit ${lastname}, ${firstname} et ${city}`)\n  }\n\n  return (\n    &lt;div>\n      &lt;h1>Mon petit formulaire à moi&lt;/h1>\n      &lt;form onSubmit={confirm}>\n        &lt;label htmlFor=\"lastname\">Nom :&lt;/label> \n        &lt;input type=\"text\" id=\"lastname\" onChange={changeLastName}/>\n        &lt;label htmlFor=\"firstname\">Prénom :&lt;/label> \n        &lt;input type=\"text\" id=\"firstname\" onChange={changeFirstName}/>\n        &lt;label htmlFor=\"city\">Ville :&lt;/label> \n        &lt;input type=\"text\" id=\"city\" onChange={changeCity}/>\n        &lt;button type=\"submit\">Valider&lt;/button>\n      &lt;/form>\n    &lt;/div>\n  );\n}\n\nexport default App;\n"
        },
        {
            "elementCategory": "text",
            "value": "La solution, c'est de simplement remplacé le useState par de simple variable comme on a appris à le faire lors de nos premiers jours d'apprentissage en développement web / programmation. Tout fonctionne exactement de la même manière. Bien entendu, si vous avez besoin d'afficher des choses différents à chaque fois que l'utilisateur tape une nouvelle lettre dans les input ou si des composants sont dépendants de ces input, alors l'utilisation du state est justifié. Mais là, pas besoin de déranger les performances de notre site."
        },
        {
            "elementCategory": "text",
            "value": "J'espère, avec cet article, vous avoir montré qu'il ne fallait pas abuser inutilement du state et que vous y voyez un peu plus clair quant à sa réelle utilisation. N'hésitez pas à m'envoyer un mail si vous avez des questions ou des suggestions. "
        }
    ],
    "readminutes": 6,
    "createDate": "02/02/2021",
    "__v": 0
}, {
    "_id": {
        "$oid": "5f23595cb389f41b6ccd2489"
    },
    "langage": [
        "css"
    ],
    "category": "Tuto",
    "title": "Bulma, framework CSS",
    "description": "Un article sur l'utilisation de Bulma, un jeune Framework CSS plein d'avenir",
    "titleLink": "bulma-framework-css",
    "intro": "Chez les développeurs fullstack, et spécialement chez ceux qui sont spécialisés dans le back, le design d\u2019une page peut être un vrai casse-tête. Mettre les mains dans le CSS ce n\u2019est pas toujours très simple. Heureusement, il existe des framework CSS tel que bootstrap ou tailwind. Celui dont j\u2019ai envie de vous parler aujourd\u2019hui, c\u2019est Bulma.",
    "article": [{
            "elementCategory": "text",
            "value": "Le but de l\u2019article n\u2019est pas de faire un comparatif avec les autres framework (pour moi ils se valent tous, faites avec celui dont vous êtes le plus à l\u2019aise). Si je vous parle de bulma, c\u2019est parce que j\u2019en suis tombé amoureux par sa simplicité. Pour vous faire partager cet amour, je vais vous montrer quelques petits trucs sympas qu\u2019on peut faire avec ce framework."
        },
        {
            "elementCategory": "subtitle",
            "value": "Bulma c\u2019est qui ?"
        },
        {
            "elementCategory": "text",
            "value": "Quand vous faites une page, il faut qu\u2019elle soit un minimum jolie, responsive et que la charte graphique soit rassurante pour l\u2019utilisateur. Par exemple, si vous faites un bouton, il faut que l\u2019utilisateur comprenne immédiatement que c\u2019est un bouton. Et pour ça, il doit correspondre aux normes graphiques de notre ère. Il y a 15 ans, on aurait fait le bouton autrement. "
        },
        {
            "elementCategory": "subtitle",
            "value": "Un bouton en 2005"
        },
        {
            "elementCategory": "img",
            "value": "buttonvieux"
        },
        {
            "elementCategory": "subtitle",
            "value": "Un bouton en 2020"
        },
        {
            "elementCategory": "img",
            "value": "buttonneuf"
        },
        {
            "elementCategory": "text",
            "value": "Mais comment, quand on n\u2019a pas fait d\u2019étude dans le design, on peut savoir si notre bouton est à la mode ou pas ? ça peut paraitre anecdotique, mais un site qui n\u2019a pas une charte graphique à la mode, c\u2019est un site qui ne donne pas très envie. C\u2019est là que les framework CSS entre en jeu, puisqu\u2019ils s\u2019occupent eux même du design."
        },
        {
            "elementCategory": "subtitle",
            "value": "Je suis beau à l\u2019intérieur"
        },
        {
            "elementCategory": "text",
            "value": "Être beau c\u2019est bien, être pratique c\u2019est bien aussi. Là où les framework CSS font fort, c\u2019est qu\u2019ils permettent aussi de rendre assez facilement votre site responsive. Que l\u2019utilisateur soit sur ordinateur, tablette ou mobile, c\u2019est le framewok qui s\u2019occupe de tout. Mais ce n\u2019est pas non plus magique, il faut préalablement lui donner des instructions. Ne vous inquiétez, on va bientôt y venir."
        },
        {
            "elementCategory": "subtitle",
            "value": "Installation de Bulma"
        },
        {
            "elementCategory": "text",
            "value": "Pour vous montrer les grandes lignes de ce framework CSS, on va créer une page HTML des plus simples avec un appel CDN vers le lien mis à disposition sur le site bulma"
        },
        {
            "elementCategory": "code",
            "value": "&lt;!DOCTYPE html>\n&lt;html lang=\"en\">\n    &lt;head>\n        &lt;meta charset=\"UTF-8\">\n        &lt;meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n        &lt;title>Bulma CSS&lt;/title>\n        &lt;link\n            rel=\"stylesheet\"\n            href=\"https://cdn.jsdelivr.net/npm/bulma@0.9.0/css/bulma.min.css\">\n    &lt;/head>\n    &lt;body>\n        &lt;h1>Djoher vous présente : Bulma CSS&lt;/h1>\n    &lt;/body>\n&lt;/html>"
        },
        {
            "elementCategory": "text",
            "value": "Si vous êtes dans un environnement un peu plus complexe (Angular, Nextjs\u2026), n\u2019oubliez pas de lire la doc pour voir comment installer Bulma. Fait un simple « npm i bulma » ne fonctionnera pas car il faut aussi appeler le module dans votre environnement. Pour ceux qui l'ignorent, npm i === npm install."
        },
        {
            "elementCategory": "link",
            "value": "https://bulma.io/documentation/overview/start/"
        },
        {
            "elementCategory": "subtitle",
            "value": "Les éléments"
        },
        {
            "elementCategory": "text",
            "value": "Vous avez besoin de boutons, de progress bar, d\u2019icon, de titres\u2026 Plus besoin de se prendre la tête, Bulma le fait pour vous. On va rester sur le fameux bouton\u2026 Imaginons qu\u2019on en a besoin, en temps normal on aurait fait : "
        },
        {
            "elementCategory": "code",
            "value": "&lt;button>Valider&lt;/button>"
        },
        {
            "elementCategory": "text",
            "value": "Ce qui aurait donné comme résultat :"
        },
        {
            "elementCategory": "html",
            "value": "&lt;button>Valider&lt;/button>"
        },
        {
            "elementCategory": "text",
            "value": "Maintenant, utilisons un peu les outils dont bulma met à disposition. Au lieu de faire comme précédemment, on va plutôt faire comme ceci :"
        },
        {
            "elementCategory": "code",
            "value": "&lt;button class=\"button\">Valider&lt;/button>"
        },
        {
            "elementCategory": "text",
            "value": "Ce qui va donner : "
        },
        {
            "elementCategory": "html",
            "value": "&lt;button class=\"button\">Valider&lt;/button>"
        },
        {
            "elementCategory": "text",
            "value": "Avouez que c\u2019est beaucoup plus dans l\u2019ère du temps non ? Au lieu de passer quelques minutes dans le fichier css, on a juste à mettre la classe « button » et voilà. Et ce n\u2019est pas tout, si on veut changer la taille du button, sa couleur ainsi que sa forme, il suffit simplement de cumuler les « class » que vous trouverez dans la doc de bulma."
        },
        {
            "elementCategory": "code",
            "value": "&lt;button class=\"button is-info is-large is-rounded\">Normal&lt;/button>"
        },
        {
            "elementCategory": "html",
            "value": "&lt;button class=\"button is-info is-large is-rounded\">Normal&lt;/button>"
        },
        {
            "elementCategory": "text",
            "value": "Bien entendu, nous pouvons étendre cet exemple sur les textes qui eux aussi, se verront modifier par rapport aux classes prédéfinis qui existent déjà sur Bulma."
        },
        {
            "elementCategory": "code",
            "value": "&lt;p class=\"has-text-link has-background-warning is-size-1\">Hello Tokyo&lt;/p>"
        },
        {
            "elementCategory": "html",
            "value": "&lt;p class=\"has-text-link has-background-warning is-size-1\">Hello Tokyo&lt;/p"
        },
        {
            "elementCategory": "code",
            "value": "&lt;p class=\"has-text-danger has-background-info is-size-7\">Hello Sapporo&lt;/p>"
        },
        {
            "elementCategory": "html",
            "value": "&lt;p class=\"has-text-danger has-background-info is-size-7\">Hello Sapporo&lt;/p>"
        },
        {
            "elementCategory": "text",
            "value": "J\u2019avoue que mes exemples sont un peu trop tape-à-l\u2019œil, mais je voulais montrer qu\u2019on peut faire pas mal de choses sans pour autant se balader dans les feuilles de style. Il n\u2019y a pas que la partie couleur ou taille qu\u2019on peut modifier avec Bulma, mais aussi la position des éléments (si vous voulez que votre texte soit centré, justifié\u2026) et même s\u2019il doit être visible ou pas dans certains cas de figure. Les possibilités sont immenses, et je vous invite à aller voir dans la documentation officielle de bulma."
        },
        {
            "elementCategory": "subtitle",
            "value": "Et le responsive dans tout ça ?"
        },
        {
            "elementCategory": "text",
            "value": "Bulma ne rend pas que les éléments jolis, il fait aussi dans le responsive. Je ne vais pas vous faire l\u2019affront de vous expliquer à quel point le responsive est important sur un site, mais de nos jours (2020), le mobile a pris la première place face au desktop. On est à l\u2019ordre du 50.13% pour les smartphone contre 47,06% pour nos chers écrans d\u2019ordinateur (et 2,81% pour le reste, dont les tablettes). Maintenant qu\u2019on est presque sur du kif-kif, il faut que notre site soit lisible partout. Bulma n\u2019est pas le seul à le faire, il y a aussi Bootstrap... Mais personnellement, je préfère bulma car il est un peu plus simple d\u2019utilisation."
        },
        {
            "elementCategory": "subtitle",
            "value": "Le point de rupture"
        },
        {
            "elementCategory": "text",
            "value": "Avant de vous montrer un peu comment bulma rend votre site internet responsive, je vais vous expliquer ce qu\u2019est un « breakpoint » afin de mieux expliquer la suite."
        },
        {
            "elementCategory": "text",
            "value": "Le point de rupture (breakpoint), c\u2019est lorsque que des éléments qui sont cote à cote doivent s\u2019empiler verticalement lorsque la taille de l\u2019écran est trop petit, afin qu\u2019ils ne débordent pas des côtés. Par exemple, si vous avez deux cadres qui font chacun 500px, il serait compliqué de les laisser cote à cote sur un mobile. Donc le moment où deux éléments changent de position, c\u2019est le breackpoint. Si je devais vous expliquer ça en image : "
        },
        {
            "elementCategory": "text",
            "value": "Le breakpoint ne s\u2019est pas encore fait car tout rentre dans l\u2019écran"
        },
        {
            "elementCategory": "img",
            "value": "sansbreak"
        },
        {
            "elementCategory": "text",
            "value": "Le breakpoint s\u2019est fait car l\u2019écran est trop petit pour laisser les deux éléments alignés."
        },
        {
            "elementCategory": "img",
            "value": "avecbreak"
        },
        {
            "elementCategory": "text",
            "value": "Pour faire un site responsive, il faut savoir utiliser les breakpoint. Maintenant, on va faire quelque chose d\u2019un peu plus complexe afin que vous voyiez comment on fait du responsive avec Bulma."
        },
        {
            "elementCategory": "subtitle",
            "value": "Voici le menu"
        },
        {
            "elementCategory": "text",
            "value": "Qui dit site, dit menu. On va donc prendre cet exemple pour montrer toute la puissance de Bulma. En vérité, Bulma peut offrir un menu préconfigurer mais on ne va pas l\u2019utiliser car ce n\u2019est pas le but de la manœuvre (même si je vous conseille de l\u2019utiliser pour votre site). "
        },
        {
            "elementCategory": "text",
            "value": "En premier lieu, on va dire à Bulma qu\u2019on va avoir une ligne avec plusieurs colonnes. Et cette ligne, c\u2019est la class « columns ». Et à l\u2019intérieur, on va mettre plusieurs colonnes avec la classe « column ». Voici une ligne avec 4 éléments :"
        },
        {
            "elementCategory": "code",
            "value": "&lt;div class=\"columns\">\n    &lt;div class=\"column has-background-primary\">\n        &lt;p class=\"has-text-centered\">News&lt;/p>\n    &lt;/div>\n    &lt;div class=\"column has-background-link\">\n        &lt;p class=\"has-text-centered\">Sorties de jeux&lt;/p>\n    &lt;/div>\n    &lt;div class=\"column has-background-info\">\n        &lt;p class=\"has-text-centered\">Test&lt;/p>\n    &lt;/div>\n    &lt;div class=\"column has-background-warning\">\n        &lt;p class=\"has-text-centered\">Forum&lt;/p>\n    &lt;/div>\n&lt;/div>\n"
        },
        {
            "elementCategory": "text",
            "value": "Remarquez que je n\u2019ai pas stipulé de taille de colonne, donc bulma repartira équitablement les 4 colonnes. Voici le résultat : "
        },
        {
            "elementCategory": "img",
            "value": "04"
        },
        {
            "elementCategory": "text",
            "value": "Comme on peut le voir, j\u2019ai centré le texte dans les colonnes et j\u2019ai mis une couleur différente fois pour qu\u2019on fasse la distinction. Vous voyez un peu l\u2019esprit. Maintenant, on va faire en sorte que la première colonne soit un peu plus grande que les autres. Pour ça, voici le code :"
        },
        {
            "elementCategory": "code",
            "value": "&lt;div class=\"columns\">\n    &lt;div class=\"column has-background-primary is-6\">\n        &lt;p class=\"has-text-centered\">News&lt;/p>\n    &lt;/div>\n    &lt;div class=\"column has-background-link is-2\">\n        &lt;p class=\"has-text-centered\">Sorties de jeux&lt;/p>\n    &lt;/div>\n    &lt;div class=\"column has-background-info is-2\">\n        &lt;p class=\"has-text-centered\">Test&lt;/p>\n    &lt;/div>\n    &lt;div class=\"column has-background-warning is-2\">\n        &lt;p class=\"has-text-centered\">Forum&lt;/p>\n    &lt;/div>\n&lt;/div>\n"
        },
        {
            "elementCategory": "text",
            "value": "ce qui donne comme résultat :"
        },
        {
            "elementCategory": "img",
            "value": "05"
        },
        {
            "elementCategory": "text",
            "value": "Ce que j\u2019ai rajouté, c\u2019est la taille des colonnes. Si vous connaissez un peu certains framework CSS, vous devriez savoir ce qu\u2019est la règle des 12. Si ce n\u2019est pas le cas, sachez qu\u2019on va simplement diviser la largeur de la page par 12 et qu\u2019on va donner une taille à chaque colonne. Dans mon code, j\u2019ai mis 6 à la première colonne et 2 aux trois autres. On peut le voir dans les class, par exemple « is-6 »."
        },
        {
            "elementCategory": "subtitle",
            "value": "Et le point de rupture ?"
        },
        {
            "elementCategory": "text",
            "value": "J\u2019y viens. Maintenant que l\u2019on sait faire des colonnes, on va configurer un peu les points de rupture (breakpoint). Par exemple, le breakpoint tablet est à 769. Cela veut dire que si votre fenêtre fait moins de 769px de largeur, alors les colonnes vont s\u2019empiler. Par exemple, là je viens de mettre ma fenêtre à 600px."
        },
        {
            "elementCategory": "img",
            "value": "06"
        },
        {
            "elementCategory": "text",
            "value": "Regardons encore un peu le texte que j\u2019avais déjà montré plus tôt."
        },
        {
            "elementCategory": "code",
            "value": "&lt;div class=\"columns\">\n    &lt;div class=\"column has-background-primary is-6\">\n        &lt;p class=\"has-text-centered\">News&lt;/p>\n    &lt;/div>\n    &lt;div class=\"column has-background-link is-2\">\n        &lt;p class=\"has-text-centered\">Sorties de jeux&lt;/p>\n    &lt;/div>\n    &lt;div class=\"column has-background-info is-2\">\n        &lt;p class=\"has-text-centered\">Test&lt;/p>\n    &lt;/div>\n    &lt;div class=\"column has-background-warning is-2\">\n        &lt;p class=\"has-text-centered\">Forum&lt;/p>\n    &lt;/div>\n&lt;/div>\n"
        },
        {
            "elementCategory": "text",
            "value": "Ce code a aussi un breakpoint mais comme vous le voyez, à aucun moment je l\u2019ai spécifié. C\u2019est normal car par défaut, le breakpoint sera en mode tablet. D\u2019ailleurs, je vais vous montrer les différents breakpoint qui existent."
        },
        {
            "elementCategory": "list",
            "value": "&lt;li>Mobile : 768px (Pas de breakpoint)&lt;/li>&lt;li>Tablet : 769px (breakpoint en dessous de 769)&lt;/li>&lt;li>Desktop : 1024px (breakpoint en dessous de 1024)&lt;/li>&lt;li>Widescreen : 1216px (breakpoint en dessous de 1216)&lt;/li>&lt;li>FullHD : 1408px (breakpoint en dessous de 1408)&lt;/li>"
        },
        {
            "elementCategory": "text",
            "value": "Si vous voulez des breakpoint vraiment précis (par exemple, un breakpoint qu\u2019entre 700px et 800px, sachez que vous pouvez configurer tout ça. Il vous suffit d\u2019aller faire un petit tour dans la documentation et faire pas mal de testes avec ce que bulma vous propose. Là je fais simplement un breakpoint desktop car je veux que les éléments s\u2019empilent lorsque ma fenêtre passe en dessous des 1024px."
        },
        {
            "elementCategory": "code",
            "value": "&lt;div class=\"columns is-desktop\">\n    &lt;div class=\"column has-background-primary is-6\">\n        &lt;p class=\"has-text-centered\">News&lt;/p>\n    &lt;/div>\n    &lt;div class=\"column has-background-link is-2\">\n        &lt;p class=\"has-text-centered\">Sorties de jeux&lt;/p>\n    &lt;/div>\n    &lt;div class=\"column has-background-info is-2\">\n        &lt;p class=\"has-text-centered\">Test&lt;/p>\n    &lt;/div>\n    &lt;div class=\"column has-background-warning is-2\">\n        &lt;p class=\"has-text-centered\">Forum&lt;/p>\n    &lt;/div>\n&lt;/div>\n"
        },
        {
            "elementCategory": "text",
            "value": "Regardez bien la première ligne, lorsqu\u2019on déclare columns dans la classe. J\u2019ai rajouté is-desktop. Et c\u2019est là toute la magie."
        },
        {
            "elementCategory": "subtitle",
            "value": "Bulma nous aime"
        },
        {
            "elementCategory": "text",
            "value": "Voilà, je vous ai présenté les deux principales utilisations de Bulma, à savoir le design et le responsive. Maintenant, il est clair que nous avons vu que la face visible de l\u2019iceberg car bulma vous propose encore pas mal d\u2019autres feature à découvrir. Même s\u2019il est encore qu\u2019en béta, il a déjà l\u2019esprit d\u2019un grand."
        }
    ],
    "readminutes": 12,
    "createDate": "15/02/2021",
    "__v": 0
}, {
    "_id": {
        "$oid": "5f29c22b9db98024c02b22af"
    },
    "langage": [
        "javascript"
    ],
    "category": "Tuto",
    "title": "Manipuler des objets avec le spread operator",
    "description": "Un article pour apprendre à manipuler les objets avec le spread operator afin de gagner du temps et quelques lignes de code",
    "titleLink": "manipuler-des-objets-avec-le-spread-operator",
    "intro": "Le spread operator (ou syntaxe de décomposition pour les amoureux de la langue française) est malheureusement trop peu utilisé alors que pour manipuler des itérables, c\u2019est aussi magique que le balai d\u2019une apprentie sorcière. Ici, on va voir comment manipuler un objet avec les spread operator pour vous faciliter la vie et alléger le nombre de ligne de vos fichiers.",
    "article": [{
            "elementCategory": "subtitle",
            "value": "E.S.6 E.S.2.0.1.5 ?"
        },
        {
            "elementCategory": "text",
            "value": "Avant de rentrer dans le vif du sujet, sachez que les spread operator ont vu le jour avec la version ES6 en 2015. C\u2019est d\u2019ailleurs pour cette raison que vous verrez souvent le ES6 sous le nom de ES2015. ES6 et ES2015, c\u2019est la même chose. Je vais certes parler des spread operator avec les objets, mais sachez que ça existe aussi pour les tableaux et les chaines de charactères. En clair, on peut utiliser les spread operator sur tout ce qui est itérable."
        },
        {
            "elementCategory": "subtitle",
            "value": "Ajouter une valeur dans un objet"
        },
        {
            "elementCategory": "text",
            "value": "Imaginons que nous avons un objet qui contient les caractéristiques d\u2019un ordinateur et que plus tard, on veut un nouvel objet en prenant les valeurs existantes de l\u2019ancien objet mais en y rajouter une valeur. Voici déjà l\u2019objet : "
        },
        {
            "elementCategory": "code",
            "value": "const computer = {\n    name : \"My PC\",\n    year : 2018,\n    processor : \"i5 7600\",\n    ram : 8,\n    graphic : \"GTX 1060\",\n    hard : \"SSD 256\"\n}\n"
        },
        {
            "elementCategory": "text",
            "value": "Ensuite, on veut créer un nouvel objet en prenant ces caractéristiques et en rajoutant une valeur."
        },
        {
            "elementCategory": "code",
            "value": "const newComputer = {\n    name : computer.name,\n    year : computer.year,\n    processor : computer.processor,\n    ram : computer.ram,\n    graphic : computer.graphic,\n    hard : computer.hard,\n    owner : \"Djoher\"\n}\n"
        },
        {
            "elementCategory": "text",
            "value": "On a donc notre nouvel ordinateur avec les anciennes données de l\u2019ancien ordinateur tout en lui ayant ajouté une valeur. Mais c\u2019est un peu long non ? On aurait pu générer ça avec le prototypage qu\u2019offre javascript, mais je ne pense pas que ça aurait été moins long. Maintenant, voyons ça avec le spread operator."
        },
        {
            "elementCategory": "code",
            "value": "const computer = {\n    name : \"My PC\",\n    year : 2018,\n    processor : \"i5 7600\",\n    ram : 8,\n    graphic : \"GTX 1060\",\n    hard : \"SSD 256\"\n}\n\nconst newComputer = {\n    ...computer,\n    owner : \"Djoher\"\n}\n"
        },
        {
            "elementCategory": "text",
            "value": "Si on demande à notre console de nous afficher l\u2019objet newComputer, voici ce qu\u2019on obtient "
        },
        {
            "elementCategory": "code",
            "value": "newComputer {\n  name: 'My PC',\n  year: 2018,\n  processor: 'i5 7600',\n  ram: 8,\n  graphic: 'GTX 1060',\n  hard: 'SSD 256',\n  owner: 'Djoher'\n}\n"
        },
        {
            "elementCategory": "text",
            "value": "C\u2019est magique non ? On vient d\u2019économiser quelques lignes de code. Ce qui vient de se passer, c\u2019est qu\u2019on vient de décomposer tout simplement l\u2019objet computer avec le spread operator."
        },
        {
            "elementCategory": "subtitle",
            "value": "Filtrer un objet "
        },
        {
            "elementCategory": "text",
            "value": "Maintenant que l\u2019on sait comment créer un nouvel objet avec de nouvelles valeurs en se basant sur un ancien objet, on va maintenant voir comment récupérer les valeurs qu\u2019on veut dans ce fameux objet computer. Notez que dans le titre, j\u2019ai bien dit « filtrer » et non « supprimer » car on ne modifie rien dans le premier objet. Si, par exemple, vous ne voulez pas le nom et l\u2019année de l\u2019ordinateur, vous pouvez faire ça "
        },
        {
            "elementCategory": "code",
            "value": "const newComputer = {\n    processor : computer.processor,\n    ram : computer.ram,\n    graphic : computer.graphic,\n    hard : computer.hard\n}\n"
        },
        {
            "elementCategory": "text",
            "value": "Encore une fois, en utilisant les spread operator, on peut faire plus simple :"
        },
        {
            "elementCategory": "code",
            "value": "const  {\n    name,\n    year,\n    ...rest\n} = computer\n"
        },
        {
            "elementCategory": "text",
            "value": "Ce qu\u2019on fait ici, c\u2019est de créer trois variables à partir de l\u2019objet computer : name, year et le reste. Et justement, ce reste est l\u2019équivalent de l\u2019objet computer mais sans les valeurs name et year, vu qu\u2019on les a isolés. Si on fait un log de rest dans notre console, on obtient "
        },
        {
            "elementCategory": "code",
            "value": "{ processor: 'i5 7600', ram: 8, graphic: 'GTX 1060', hard: 'SSD 256' }"
        },
        {
            "elementCategory": "text",
            "value": "Notez que j\u2019ai utilisé le nom « rest » comme variable mais vous pouvez mettre ce que vous voulez. Rest, c\u2019est juste une convention."
        },
        {
            "elementCategory": "subtitle",
            "value": "Et pour les objets dans des objets ?"
        },
        {
            "elementCategory": "text",
            "value": "Rien de plus simple, c\u2019est presque la même chose. Pour vous montrer, je vais faire un nouvel exemple de l\u2019objet computer"
        },
        {
            "elementCategory": "code",
            "value": "const computer = {\n    data: {\n        name: \"My PC\",\n        year: 2018,\n        processor: \"i5 7600\",\n        ram: 8,\n        graphic: \"GTX 1060\",\n        hard: \"SSD 256\"\n    }\n}\n"
        },
        {
            "elementCategory": "text",
            "value": "Et pour créer un nouvel objet en y ajoutant une nouvelle valeur "
        },
        {
            "elementCategory": "code",
            "value": "const newComputer = {\n    ...computer, \n    data : {\n        ...computer.data,\n        owner : \"Djoher\"\n    }\n}\n"
        },
        {
            "elementCategory": "subtitle",
            "value": "Fusionnons les valeurs"
        },
        {
            "elementCategory": "text",
            "value": "Maintenant qu\u2019on sait ajouter des valeurs et ne prendre que les valeurs qui nous intéresse, voyons voir comme on va faire pour faire la fusion de valeur. On va laisser tomber l\u2019exemple des ordinateurs et revenir vers quelque chose de plus simple. "
        },
        {
            "elementCategory": "text",
            "value": "La ville et le numéro de téléphone seront absents, mais on peut faire en sorte qu\u2019ils soient présents dans le nouvel objet."
        },
        {
            "elementCategory": "code",
            "value": "const obj1 = {\n    name : \"Djoher\",\n    age : 35,\n    city : null,\n    telephone : null\n}\nconst obj2 = {\n    city : \"paris\",\n    telephone : 123456\n}\n\nconst obj3 = {\n    ...obj1,\n    ...obj2\n}\n"
        },
        {
            "elementCategory": "text",
            "value": "Voici le résultat quand on log la variable obj3 dans la console : "
        },
        {
            "elementCategory": "code",
            "value": "{ name: 'Djoher', age: 35, city: 'paris', telephone: 123456}"
        },
        {
            "elementCategory": "text",
            "value": "Et voilà, on a la fusion des deux objets ! Mais une question se pose : comment sait-on si la bonne valeur est « null » ou « paris » pour la ville ? Et pour le numéro de téléphone ? En fait, c\u2019est dans l\u2019ordre qu\u2019on a stipulé lors de la création de la variable obj3. Si on avait fait plutôt comme ça : "
        },
        {
            "elementCategory": "code",
            "value": "const obj3 = {\n    ...obj2,\n    ...obj1\n}\n"
        },
        {
            "elementCategory": "text",
            "value": "On aurait eu ce résultat : "
        },
        {
            "elementCategory": "code",
            "value": "{ city: null, telephone: null, name: 'Djoher', age: 35 }"
        },
        {
            "elementCategory": "text",
            "value": "Voilà, nous avons maintenant vu comment manipuler les spread operator pour les objets. En ce qui concerne les tableaux ou les chaines de charactères, vous verrez que ce n\u2019est pas si différent."
        }
    ],
    "readminutes": 8,
    "createDate": "25/02/2021",
    "__v": 0
}, {
    "_id": {
        "$oid": "5f343311de32bd415cc3009c"
    },
    "langage": [
        "node"
    ],
    "category": "Astuce",
    "title": "Sécurisez vos routes Node",
    "description": "Un article qui vous donnes les bases pour sécurisez vos routes afin que des personnes mal-intentionnées ne fassent pas planter votre serveur",
    "titleLink": "securisez-vos-routes-node",
    "intro": "Quand on lance notre application Node et que celle-ci partage des informations via ses routes API, on est toujours tout content et on passe généralement à autre chose. Mais quand on passe en production et que notre application est utilisée par des milliers de personnes, il vaut mieux avoir quelques bases en ce qui concerne la sécurité. Sinon PAF, plus de serveur.",
    "article": [{
            "elementCategory": "text",
            "value": "Internet est une jungle dont les arbres peuvent parfois cacher de vilaines personnes qui n\u2019hésiteront pas à faire tomber votre application Node. Et ça c\u2019est dans les meilleurs des cas. Si ces personnes sont vraiment malveillantes, ils peuvent voler pas mal d\u2019informations qui ne devrait pas être disponible à qui veut. Cet article vous donnera quelques astuces afin de vous protéger et ne pas se retrouver avec une application prise d\u2019assaut par des hackers. Bien entendu, si vous bossez pour une grosse multinationale qui pèse des milliards, je vous conseille de voir au-delà de cet article."
        },
        {
            "elementCategory": "text",
            "value": "Dernière petite précision, je pars du principe que vous utilisez Express."
        },
        {
            "elementCategory": "subtitle",
            "value": "Sois dans les temps"
        },
        {
            "elementCategory": "text",
            "value": "La première chose dont vous devez faire attention, c\u2019est que votre application Node / Express ne soit pas dans une version obsolète. Ça parait bête, mais sachez que si votre version date de mathusalem, il y a de fortes chances pour que les failles de sécurité connues ne soient plus corrigées par l\u2019équipe de développement Node. Donc c\u2019est à vous de voir si c\u2019est intéressant de passer à la version supérieure ou pas."
        },
        {
            "elementCategory": "text",
            "value": "Pour voir la version node qui est installé dans votre machine, il vous suffit de taper"
        },
        {
            "elementCategory": "code",
            "value": "$ node -v"
        },
        {
            "elementCategory": "text",
            "value": "N\u2019hésitez pas à vérifier sur le site officiel les recommandations en ce qui concerne votre version. Personnellement, je ne vous conseille pas de prendre la dernière version mais celle qui est recommandé pour la plupart des gens. Et n\u2019oubliez pas de tout tester pour voir si rien n\u2019a été cassé dans l\u2019upgrade version."
        },
        {
            "elementCategory": "subtitle",
            "value": "Helmet"
        },
        {
            "elementCategory": "text",
            "value": "Ce module très célèbre est en fait un regroupement de plusieurs petits modules qui permettront de sécuriser votre HTTP headers. Pour l\u2019installer, il faut faire"
        },
        {
            "elementCategory": "code",
            "value": "$ npm i helmet"
        },
        {
            "elementCategory": "text",
            "value": "Et pour l\u2019utiliser"
        },
        {
            "elementCategory": "code",
            "value": "const express = require(\"express\");\nconst helmet = require(\"helmet\");\n \nconst app = express();\n \napp.use(helmet());\n"
        },
        {
            "elementCategory": "text",
            "value": "Maintenant, vous avez pas mal de défense en ce qui concerne les informations présentes dans le Headers. Vous pouvez aussi faire une configuration plus précise pour Helmet, mais je vous conseille de savoir exactement ce que vous faites. "
        },
        {
            "elementCategory": "subtitle",
            "value": "XSS"
        },
        {
            "elementCategory": "text",
            "value": "Avant de vous dire comment vous protéger contre les attaques XSS, il serait sage que je vous explique ce que c\u2019est. Une attaque XSS, c\u2019est l\u2019injection d\u2019un script dans votre serveur, ce qui modifiera son comportement. Si par exemple dans un formulaire, une personne envoie une longue ligne de code à la place de son prénom, le serveur peut croire que c\u2019est une ligne de code à lancer. Et à ce moment-là, c\u2019est open bar pour les script externes."
        },
        {
            "elementCategory": "text",
            "value": "Mais pas de panique, ces attaques sont connues dans le monde du dev et plusieurs solutions existent déjà. La plus simple pour votre application node est d\u2019installer un module qui va gérer pour vous. Un des plus connu, c\u2019est XSS."
        },
        {
            "elementCategory": "code",
            "value": "$ npm install xss"
        },
        {
            "elementCategory": "code",
            "value": "const xss = require(\"xss\");\nconst html = xss('&lt;script>alert(\"xss\");&lt;/script>');\nconsole.log(html);\n"
        },
        {
            "elementCategory": "subtitle",
            "value": "express-rate-limit"
        },
        {
            "elementCategory": "text",
            "value": "Si une personne veut faire tomber un serveur, il peut envoyer des requêtes en boucle. Ce qui aura comme conséquence de faire stresser votre application Node. Un moyen pour empêcher ce genre de pratique est de limiter le nombre de requête que peut faire un client. Le module que j\u2019utilise personnellement est express-rate-limit"
        },
        {
            "elementCategory": "code",
            "value": "$ npm install --save express-rate-limit"
        },
        {
            "elementCategory": "code",
            "value": "const rateLimit = require(\"express-rate-limit\");\n\nconst limiter = rateLimit({\n  windowMs: 10 * 60 * 1000, // Voici l\u2019équivalent de 10 minutes\n  max: 100 // Le client pourra donc faire 100 requêtes toutes les 10 minutes\n});\n \n//  Cette limite de 100 requêtes toutes les 10 minutes sera effective sur toutes les routes.\napp.use(limiter);\n"
        },
        {
            "elementCategory": "text",
            "value": "Si vous souhaitez que chaque route ait son propre limiteur, vous pouvez faire comme ça "
        },
        {
            "elementCategory": "code",
            "value": "const rateLimit = require(\"express-rate-limit\");\n\nconst apiLimiter1 = rateLimit({\n  windowMs: 10 * 60 * 1000, \n  max: 100\n});\n \napp.use(\"/api/test1\", apiLimiter);\n\nconst apiLimiter2 = rateLimit({\n  windowMs: 25 * 60 * 1000, \n  max: 150\n});\n\napp.use(\"/api/test2\", apiLimiter2);\n"
        },
        {
            "elementCategory": "subtitle",
            "value": "IP"
        },
        {
            "elementCategory": "text",
            "value": "Imaginons que vous avez un serveur Node qui doit envoyer des données à votre seule application Front, ça implique donc que si une autre adresse IP que votre application Front demande des infos, votre Node doit lui refuser l\u2019accès. Pour ça, nous avons le module IP. Très pratique, il permet de récupérer l\u2019adresse IP de la demande et de faire des comparaisons / validations."
        },
        {
            "elementCategory": "code",
            "value": "$ npm install ip"
        },
        {
            "elementCategory": "text",
            "value": "Afin de faire votre opération, ce module offre pas mal de méthode qui vous permet de faire ce que vous voulez. Voici un petit copier de ce qu\u2019on trouve sur le site github du module :"
        },
        {
            "elementCategory": "code",
            "value": "var ip = require('ip');\n\nip.address() // my ip address\nip.isEqual('::1', '::0:1'); // true\nip.toBuffer('127.0.0.1') // Buffer([127, 0, 0, 1])\nip.toString(new Buffer([127, 0, 0, 1])) // 127.0.0.1\nip.fromPrefixLen(24) // 255.255.255.0\nip.mask('192.168.1.134', '255.255.255.0') // 192.168.1.0\nip.cidr('192.168.1.134/26') // 192.168.1.128\nip.not('255.255.255.0') // 0.0.0.255\nip.or('192.168.1.134', '0.0.0.255') // 192.168.1.255\nip.isPrivate('127.0.0.1') // true\nip.isV4Format('127.0.0.1'); // true\nip.isV6Format('::ffff:127.0.0.1'); // true\n\n// operate on buffers in-place\nvar buf = new Buffer(128);\nvar offset = 64;\nip.toBuffer('127.0.0.1', buf, offset);  // [127, 0, 0, 1] at offset 64\nip.toString(buf, offset, 4);            // '127.0.0.1'\n\n// subnet information\nip.subnet('192.168.1.134', '255.255.255.192')\n// { networkAddress: '192.168.1.128',\n//   firstAddress: '192.168.1.129',\n//   lastAddress: '192.168.1.190',\n//   broadcastAddress: '192.168.1.191',\n//   subnetMask: '255.255.255.192',\n//   subnetMaskLength: 26,\n//   numHosts: 62,\n//   length: 64,\n//   contains: function(addr){...} }\nip.cidrSubnet('192.168.1.134/26')\n// Same as previous.\n\n// range checking\nip.cidrSubnet('192.168.1.134/26').contains('192.168.1.190') // true\n\n\n// ipv4 long conversion\nip.toLong('127.0.0.1'); // 2130706433\nip.fromLong(2130706433); // '127.0.0.1'\n"
        },
        {
            "elementCategory": "text",
            "value": "Avec ça, vous devrez normalement trouver votre bonheur."
        },
        {
            "elementCategory": "subtitle",
            "value": "I\u2019m so secure / insecure"
        },
        {
            "elementCategory": "text",
            "value": "Normalement, si vous utilisez bien les outils plus haut, vous devriez avoir une bonne base pour ne pas vous faire attaquer par des hacker sans scrupules qui prennent plaisir à faire tomber les serveurs. Mais n\u2019oubliez jamais que la meilleure protection pour votre serveur, c\u2019est ce que vous faites, pas ce que vous utilisez. "
        }
    ],
    "readminutes": 5,
    "createDate": "03/03/2021",
    "__v": 0
}, {
    "_id": {
        "$oid": "5f576ca87d1e326a80c62254"
    },
    "langage": [
        "mongo"
    ],
    "category": "Astuce",
    "title": "Relations avec MongoDB",
    "description": "Un article qui parle des relations de base de données avec MongoDB",
    "titleLink": "relations-avec-mongodb",
    "intro": "Quand vous créez une base de données, vous devez faire attention à plusieurs problématiques, dont sa légèreté et sa compréhension rapide. Et la chose qu\u2019il ne faut surtout pas faire, c\u2019est des répétitions d\u2019objet car non seulement ça alourdit la base, mais ça devient très compliqué en cas de modification d\u2019objet. Aujourd\u2019hui, on va voir comment faire une association d\u2019objet dans d\u2019autres objets avec MongoDB. Ça parait compliqué mais vous allez voir, c\u2019est plus simple qu\u2019il n\u2019y parait.",
    "article": [{
            "elementCategory": "subtitle",
            "value": "Assemble "
        },
        {
            "elementCategory": "text",
            "value": "Comme à mon habitude, je vais expliquer le concept avec un exemple des plus basiques pour ne pas vous perdre dès le premier paragraphe."
        },
        {
            "elementCategory": "text",
            "value": "Imaginons que nous nous occupons d\u2019un site où des utilisateurs peuvent lire des manga, cela veut dire qu\u2019on a une base de donnée qui contiendra une collection d\u2019utilisateur et une collection de manga. Si on devait schématiser, voici ce qu\u2019on obtiendrait :"
        },
        {
            "elementCategory": "code",
            "value": "{\n  \"_id\" : ObjectId(\"541fs5dfdsf546sd2f1ds\"),\n  \"name\" : \"Ryan\",\n  \"email\" : \"jaimelesmanga@gmail.com\",\n  \"lang\" : \"fr\"\n  \"zipcode\": 75001,\n  \"mangaAlreadyRead\": []\n}\n"
        },
        {
            "elementCategory": "text",
            "value": "Très basique. Maintenant, place au schéma du manga :"
        },
        {
            "elementCategory": "code",
            "value": "{\n  \"_id\" : ObjectId(\"8d7f87ds96s66ssfddf\"),\n  \"title\":\"Berserk\",\n  \"author\": \"Kentaro Miura\",\n  \"tome\": 40\n}\n"
        },
        {
            "elementCategory": "text",
            "value": "Là non plus, pas besoin de faire compliquer. Pour les plus novice d\u2019entres nous, sachez que le document « _id » a été crée automatiquement par MongoDB lors de la création de l'objet, afin qu\u2019il puisse avoir un document unique et éviter les doublons. Si, pour une raison bizarre, on veut créer le même objet avec les mêmes infos, voici comment MongoDB l\u2019intégrerait dans sa base :"
        },
        {
            "elementCategory": "code",
            "value": "{\n  \"_id\" : ObjectId(\"8d7f87ds96s66ssfddf\"),\n  \"title\":\"Berserk\",\n  \"author\": \"Kentaro Miura\",\n  \"tome\":40\n}\n"
        },
        {
            "elementCategory": "code",
            "value": "{\n  \"_id\" : ObjectId(\"43d45s64dsf1215e6z6\"),\n  \"title\":\"Berserk\",\n  \"author\": \"Kentaro Miura\",\n  \"tome\":40\n}\n"
        },
        {
            "elementCategory": "text",
            "value": "Vous voyez ? MongoDB s\u2019est occupé de mettre un « _id » différent pour qu\u2019il puisse les différencier. Bref, revenons à nos moutons."
        },
        {
            "elementCategory": "text",
            "value": "Imaginons maintenant que Ryan (oui c\u2019est moi) vient de lire le manga en question, il faut que la base de données le sache pour, par exemple, ne pas lui reproposer de lire ce manga sur le site. Si vous jetez encore un coup d\u2019œil à l\u2019objet de l\u2019utilisateur, vous remarquerez un document qui se nomme « mangaAlreadyRead ». C\u2019est ici que nous allons noter tous les manga qui ont déjà été lu par Ryan."
        },
        {
            "elementCategory": "subtitle",
            "value": "Ce qu\u2019il ne faut pas faire"
        },
        {
            "elementCategory": "text",
            "value": "Nous sommes donc tentés de mettre une copie de l\u2019objet du manga Berserk dans le document « mangaAlreadyRead » comme ceci :"
        },
        {
            "elementCategory": "code",
            "value": "{\n    \"_id\" : ObjectId(\"541fs5dfdsf546sd2f1ds\"),\n    \"name\" : \"Ryan\",\n    \"email\" : \"jaimelesmanga@gmail.com\",\n    \"lang\" : \"fr\"\n    \"zipcode\": 75001,\n    \"mangaAlreadyRead\": [{\n      \"_id\" : ObjectId(\"8d7f87ds96s66ssfddf\"),\n      \"title\":\"Berserk\",\n      \"author\": \"Kentaro Miura\",\n      \"tome\":40\n    }]\n  }\n"
        },
        {
            "elementCategory": "text",
            "value": "Avec l\u2019objet dans le tableau « mangaAlreadyRead », on pourra savoir que Ryan a lu ce manga. Dans l\u2019idée c\u2019est juste, mais dans la pratique c\u2019est pas bon."
        },
        {
            "elementCategory": "text",
            "value": "Si on fait comme ça, on aura un doublon de l\u2019objet du manga Berserk. Non seulement il existera dans la collection manga (là où il doit être) mais aussi dans la collection de l\u2019utilisateur, plus précisément dans « mangaAlreadyRead ». Vous ne voyez pas encore le souci ?"
        },
        {
            "elementCategory": "text",
            "value": "Maintenant imaginons que l\u2019on doive modifier l\u2019objet du manga car un nouveau tome vient d\u2019être disponible, il faudra le modifier dans la collection manga\u2026 mais aussi chez tous les utilisateurs qui ont lu le manga. Et ça, croyez-moi, vous n\u2019avez pas envie de le faire. C\u2019est là que les associations entrent en jeu."
        },
        {
            "elementCategory": "subtitle",
            "value": "Ce qu\u2019il faut faire"
        },
        {
            "elementCategory": "text",
            "value": "C\u2019est ici que le document « _id » est très utile car au lieu de mettre tout l\u2019objet du manga dans l\u2019objet de l\u2019utilisateur, on va juste mettre ce fameux _id."
        },
        {
            "elementCategory": "code",
            "value": "{\n    \"_id\" : ObjectId(\"541fs5dfdsf546sd2f1ds\"),\n    \"name\" : \"Ryan\",\n    \"email\" : \"jaimelesmanga@gmail.com\",\n    \"lang\" : \"fr\"\n    \"zipcode\": 75001,\n    \"mangaAlreadyRead\": [ ObjectId(\"8d7f87ds96s66ssfddf\")]\n  }\n"
        },
        {
            "elementCategory": "text",
            "value": "Maintenant, la liaison est faite entre l\u2019utilisateur et le manga. Et quand on modifiera le manga pour augmenter ses tomes, on n\u2019aura plus besoin de toucher à autre chose. Comme vous pouvez le voir, « mangaAlreadyRead » est un tableau, donc on peut lui mettre autant de manga qu\u2019on veut. Pratique non ?"
        },
        {
            "elementCategory": "text",
            "value": "En conclusion, on a juste besoin de mettre le « _id » automatiquement crée par MongoDB et c\u2019est lui qui gérera le reste."
        },
        {
            "elementCategory": "text",
            "value": "Si, comme moi, vous êtes un grand utilisateur de Mongoose, savez que la logique est la même, sauf la syntaxe qui sera un peu différente. Mais dans l\u2019esprit, c\u2019est la même chose."
        },
        {
            "elementCategory": "text",
            "value": "J\u2019espère vous avoir aidé à y voir plus claire dans l\u2019utilisation optimale des bases de données. Je vous donne rendez-vous une prochaine fois pour de nouvelles aventures. "
        }
    ],
    "readminutes": 4,
    "createDate": "16/03/2021",
    "__v": 0
}, {
    "_id": {
        "$oid": "5f590d149452bc4ad8f1ad33"
    },
    "langage": [
        "git"
    ],
    "category": "Tuto",
    "title": "Les commandes basiques de GIT",
    "description": "Ici, vous apprendrez à faire les commandes GIT les plus basiques, mais aussi les plus utilisés",
    "titleLink": "les-commandes-basiques-de-git",
    "intro": "Je pense que je n\u2019ai pas besoin de vous le dire, mais GIT est un outil très puissant qui va vous permettre de faire de grandes choses. Son principal souci, c\u2019est que les néophytes ont énormément peur de l\u2019utiliser. Pas parce qu\u2019il est très compliqué (quoique), mais surtout parce qu\u2019une mauvaise manipulation peut mettre en mal l\u2019organisation du projet (même si c\u2019est faux, on peut toujours revenir en arrière). Aujourd\u2019hui, on va voir les principales commandes de GIT.",
    "article": [{
            "elementCategory": "subtitle",
            "value": "Un projet à gérer"
        },
        {
            "elementCategory": "text",
            "value": "Avant de s\u2019occuper des commandes GIT, il nous faut un projet à gérer. Pas besoin de faire un énorme dossier avec plusieurs sous-dossiers, vous pouvez même gérer les versions d\u2019un simple fichier texte. Mais faisons comme si on avait un projet web. PS: Je pars du principe que vous êtes sur windows mais je sais que ce n'est pas le cas pour certains. Je m'en excuse d'avance."
        },
        {
            "elementCategory": "img",
            "value": "git01"
        },
        {
            "elementCategory": "text",
            "value": "Maintenant, il est temps de jouer avec GIT. Comme partout, il faut d\u2019abord s\u2019identifier car si on s\u2019amuse à faire des commit sans dire qui on est, ça risque de poser problème pour savoir qui a fait quoi. Pas besoin de mettre son âge ou sa taille, son nom et son email suffiront. En ligne de commande, voici les deux choses à taper :"
        },
        {
            "elementCategory": "code",
            "value": "git config --global user.name \"Ryan\" "
        },
        {
            "elementCategory": "code",
            "value": "git config --global user.email \"Ryan@hotmailetgmail.com\""
        },
        {
            "elementCategory": "text",
            "value": "Notez qu\u2019il y a « global » dans les lignes de commandes. Ça veut tout simplement dire que ces identifiants fonctionneront pour en global sur votre ordinateur. Donc vous n\u2019aurez plus besoin de faire ça si vous gérez un autre projet."
        },
        {
            "elementCategory": "subtitle",
            "value": "Initialisation de GIT"
        },
        {
            "elementCategory": "text",
            "value": "Maintenant, en étant dans le dossier visé, on créer une initialisation de GIT avec cette ligne de commande :"
        },
        {
            "elementCategory": "code",
            "value": "git init"
        },
        {
            "elementCategory": "text",
            "value": "Normalement, le message suivant s\u2019affiche : Initialized empty Git repository in C:/\u2026 . Félicitations, GIT va maintenant tracer votre dossier. Mais comment fait-il ? C\u2019est simple, un dossier caché a été créé dans le projet (.git) et c\u2019est dans ce dossier que seront stockés les infos."
        },
        {
            "elementCategory": "img",
            "value": "git02"
        },
        {
            "elementCategory": "text",
            "value": "Laissez ce dossier tranquille. Si vous être débutant, ne vous amusez surtout pas à modifier les fichiers."
        },
        {
            "elementCategory": "subtitle",
            "value": "Voir les modifications"
        },
        {
            "elementCategory": "text",
            "value": "La commande GIT que vous utiliserez le plus au cours de votre vie, c\u2019est cette commande : "
        },
        {
            "elementCategory": "code",
            "value": "git status"
        },
        {
            "elementCategory": "text",
            "value": "Cette commande magique vous permet de voir la statue de votre dossier GIT : Si des fichiers ont été modifiés, s\u2019ils ont été commité ou pas, s\u2019ils sont supprimés\u2026 Par exemple, si à ce stade je fais un « git status », j\u2019obtiens ce message : "
        },
        {
            "elementCategory": "img",
            "value": "git03"
        },
        {
            "elementCategory": "text",
            "value": "« On branch master » veut dire qu\u2019on se trouve actuellement sur la branche master. « No commits yet » veut dire qu\u2019on n\u2019a encore pas de commit en attente. Ensuite, c\u2019est là que ça devient intéressant, c\u2019est la liste qui se trouve sous le « Untracked files ». On voit clairement la liste des dossiers en rouges, cela veut dire que GIT les voit, mais que pour l\u2019instant ils ne sont pas encore tracés. Et c\u2019est justement ce dont on va s\u2019occuper juste après."
        },
        {
            "elementCategory": "subtitle",
            "value": "Ajout des dossiers"
        },
        {
            "elementCategory": "text",
            "value": "S\u2019il te plait GIT, peux-tu tracer le dossier « components » ? Comme rien n\u2019est magique, il va falloir le faire soi-même en ligne de commande :"
        },
        {
            "elementCategory": "code",
            "value": "git add component/"
        },
        {
            "elementCategory": "img",
            "value": "git04"
        },
        {
            "elementCategory": "text",
            "value": "Même s\u2019il ne nous affiche pas de message de confirmation, tout s\u2019est bien déroulé. Mais pour en être sûr, n\u2019oubliez pas cette commande très utile :"
        },
        {
            "elementCategory": "code",
            "value": "git status"
        },
        {
            "elementCategory": "text",
            "value": "Vous voyez que la ligne « component » et ce qu\u2019elle contient (juste un fichier) est en vert. Le dossier est donc ajouté et n\u2019attend plus qu\u2019à être commit. Mais si au final on a décidé de mettre tous les dossiers, on peut toujours faire :"
        },
        {
            "elementCategory": "code",
            "value": "git add ."
        },
        {
            "elementCategory": "text",
            "value": "Le point signifie que tout ce qui n\u2019est pas encore tracké par GIT le sera. Si on regarde maintenant notre console avec un « git status », nos lignes qui étaient en rouge passeront en vert. La suite, c\u2019est de commit tout ça."
        },
        {
            "elementCategory": "subtitle",
            "value": "Commit"
        },
        {
            "elementCategory": "text",
            "value": "Nous allons donc sauvegarder tout ça dans un joli commit. Pour ça, il suffit simplement de taper cette commande :"
        },
        {
            "elementCategory": "code",
            "value": "git commit -m \"mon premier commit\""
        },
        {
            "elementCategory": "text",
            "value": "« mon premier commit » est le nom du commit qu\u2019on lui donnera. Je vous déconseille d\u2019utiliser des phrases génériques du type « save ». Il faut être le plus précis possible pour ne pas être perdu en cas de problème. N\u2019utilisez pas GIT pour juste sauvegarder vos travaux. Il existe d\u2019autres outils pour ça."
        },
        {
            "elementCategory": "subtitle",
            "value": "Back to the past"
        },
        {
            "elementCategory": "text",
            "value": "Il est possible qu\u2019après avoir fait votre commit, vous vouliez revenir en arrière et annuler ce même commit. Pas de soucis, la commande est très simple :"
        },
        {
            "elementCategory": "code",
            "value": "git reset --soft HEAD~1"
        },
        {
            "elementCategory": "text",
            "value": "Votre dernier commit a donc été annulé. Vous pouvez faire vos modifications avant de refaire un commit."
        },
        {
            "elementCategory": "subtitle",
            "value": "Dans le cloud"
        },
        {
            "elementCategory": "text",
            "value": "Si comme la plupart des gens, vous envoyez vos travaux sur un serveur externe, alors il va falloir le préciser à GIT. Imaginons que vous avez crée un repository sur le célèbre site Gitlab, normalement il devrait vous donner un lien qui ressemble à ça :"
        },
        {
            "elementCategory": "code",
            "value": "git remote add origin git@gitlab.com:lesitededjoher/djoher.git"
        },
        {
            "elementCategory": "text",
            "value": "Et c\u2019est exactement cette commande qu\u2019il faudra rentrer dans votre ligne de commande pour que GIT sache que vous avez un repository externe dans le cloud qui n\u2019attend que vos travaux. La dernière chose à faire, c\u2019est d\u2019envoyer votre commit là-bas."
        },
        {
            "elementCategory": "code",
            "value": "git push origin master"
        },
        {
            "elementCategory": "text",
            "value": "Cette commande envoie votre commit sur le site. Et si vous regardez bien, il envoie ça dans la branche « master ». Félicitation, vos travaux sont sur le cloud, mais il est temps maintenant de vous parler un peu des branches."
        },
        {
            "elementCategory": "subtitle",
            "value": "Les branches"
        },
        {
            "elementCategory": "text",
            "value": "Les branches vont vous permettre d\u2019avoir un peu plus de sureté dans votre projet. Généralement, on n\u2019envoie pas sur la branche « master » car elle doit être clean. Vous devriez avoir une branche qui porte le nom de votre feature (voire juste vote nom/prénom). La personne responsable du repository s\u2019occupera de fusionner les branches quand il jugera bon de le faire."
        },
        {
            "elementCategory": "text",
            "value": "Bref, nous sommes toujours sur la branche master mais on va y partir tout en créant une nouvelle branche. Pour ça, on va faire cette commande :"
        },
        {
            "elementCategory": "code",
            "value": "git branch my_new_banch"
        },
        {
            "elementCategory": "text",
            "value": "Votre nouvelle branch portera le nom de « my_new_branch ». Mais elle est seulement créée, on n\u2019est pas encore dessus. Il faut bien faire attention à regarder sur quelle branche on se trouve. Pour ça, pas besoin de faire un « git status », il suffit juste d\u2019avoir l\u2019œil. "
        },
        {
            "elementCategory": "img",
            "value": "git05"
        },
        {
            "elementCategory": "text",
            "value": "C\u2019est bien tout ça, mais maintenant il faut changer de branche. Avant de bouger, voici une commande qui vous permet de voir la liste des branch que vous avez en local (et en remote, on y reviendra) sur votre projet :"
        },
        {
            "elementCategory": "code",
            "value": "git branch -a"
        },
        {
            "elementCategory": "img",
            "value": "git06"
        },
        {
            "elementCategory": "text",
            "value": "Sur cette image, vous voyez que j\u2019ai deux branches : master et my_new_branch. Vous voyez aussi que master est en vert, cela signifie que c\u2019est sur cette branche qu\u2019on est actuellement. Fini le suspense, voici maintenant la commande pour changer de branche :"
        },
        {
            "elementCategory": "code",
            "value": "git checkout my_new_branch"
        },
        {
            "elementCategory": "img",
            "value": "git07"
        },
        {
            "elementCategory": "text",
            "value": "Non seulement on voit le message de confirmation qui nous dit qu\u2019on a bien changé de branche, mais en plus on voit (my_new_branch) sur notre ligne. Félicitation, vous être enfin dans votre branche."
        },
        {
            "elementCategory": "text",
            "value": "Attention, n\u2019oubliez pas que vous avez crée une branche en local, donc seulement présente dans votre ordinateur. Tant que vous n\u2019avez pas fait un push sur votre repository présent sur le site type github ou gitlab, elle sera juste sur votre ordinateur. Et c\u2019est aussi le cas des branches qui sont sur le site mais pas sur votre ordinateur. Normalement, quand vous ferez la commande « git branch -a », vous devriez voir les branches présentes en local, mais aussi celle qui sont sur le site. Vous savez maintenant comment changer de branche donc vous pouvez aussi vous mettre dans une branche qui n\u2019est pas encore présent en local dans votre ordinateur. Et quand c\u2019est fait, il faut télécharger les fichiers pour que vous ayez les mêmes fichiers de la branche, que ce soit sur le site ou sur votre ordinateur. Pour ça : "
        },
        {
            "elementCategory": "code",
            "value": "git pull"
        },
        {
            "elementCategory": "subtitle",
            "value": "Ce n\u2019est que le début"
        },
        {
            "elementCategory": "text",
            "value": "Vous savez maintenant les bases de GIT mais je vous conseille non seulement de creuser un peu plus le sujet, mais aussi de vous entrainez encore et encore. Quand vous serez à l\u2019aise avec les lignes de commandes et tout l\u2019esprit derrière GIT, vous aurez eu un gain d\u2019expérience non négligeable. Sur ce, je vous souhaite une « git pull bonne_journée »."
        }
    ],
    "readminutes": 7,
    "createDate": "02/04/2021",
    "__v": 0
}, {
    "_id": {
        "$oid": "5f69db501dbf8a64245bc4c0"
    },
    "langage": [
        "socketio"
    ],
    "category": "Tuto",
    "title": "Socket io, des données en temps réelle",
    "description": "Cet article vous montre les différentes possibilités qui vous sont offertes avec Socket-io quand il sagit de faire du temps-réel",
    "titleLink": "socket-io-des-donnees-en-temps-reelle",
    "intro": "Quand on débute dans le développement web, on pense que pour changer les données d\u2019une page, on doit forcément la rafraichir afin que le serveur ait le temps de faire sa petite mise à jour. Puis un jour on découvre Socket.io, et le dynamisme d\u2019une page prend tout son sens. Le temps réel, c\u2019est l\u2019avenir.",
    "article": [{
            "elementCategory": "subtitle",
            "value": "Et pourquoi ça ?"
        },
        {
            "elementCategory": "text",
            "value": "Si on veut afficher les données d\u2019un utilisateur, on n\u2019a franchement pas besoin d\u2019utiliser du temps réel puisque aucun changement inopiné ne viendra perturber tout ça. Mais dans le cas où quelqu\u2019un d\u2019extérieur doit changer des informations sur votre page, Socket.io prend tout son sens. Vous l\u2019aurez surement deviné, je parle des chats. Quand on est sur un site de vente et qu\u2019on veut discuter avec un téléconseiller, on peut avoir une discussion sans être interrompu par notre navigateur qui veut son petit reload. C\u2019est dans ce genre de cas de figure où Socket.io est vraiment pratique. Mais attention, il ne faut pas en abuser car en termes de ressources, c\u2019est assez gourmand."
        },
        {
            "elementCategory": "subtitle",
            "value": "Installation de Socket.io"
        },
        {
            "elementCategory": "text",
            "value": "Pour illustrer mon petit tuto, on va faire un serveur très simpliste en Node. Normalement, vous devriez savoir ce que chaque ligne fait."
        },
        {
            "elementCategory": "code",
            "value": "const express = require('express');\nconst app = express();\nconst PORT = 8889;\n\napp.get('/', (req, res) => {\n    res.sendFile(__dirname + '/index.html');\n});\n\napp.listen(PORT, () => console.log(`Le serveur est sur le http://localhost:${PORT}/`));\n"
        },
        {
            "elementCategory": "text",
            "value": "Donc si on lance notre serveur (après avoir fait notre petit installation de express via la commande $ npm i express --save), la racine de notre site devrait nous renvoyer la page index.html que voici : "
        },
        {
            "elementCategory": "code",
            "value": "&lt;!DOCTYPE html>\n&lt;html lang=\"en\">\n\n&lt;head>\n    &lt;meta charset=\"UTF-8\">\n    &lt;meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n    &lt;title>Tuto Socket.io&lt;/title>\n&lt;/head>\n\n&lt;body>\n    &lt;h1>Salut Socket.io&lt;/h1>\n&lt;/body>\n\n&lt;/html>\n"
        },
        {
            "elementCategory": "text",
            "value": "La première chose qu\u2019on va voir, c\u2019est quand le serveur va envoyer un message au client sans rechargement de page. Donc pour l\u2019instant, on installe Socket.Io via cette commande :"
        },
        {
            "elementCategory": "code",
            "value": "$ npm install socket.io --save "
        },
        {
            "elementCategory": "text",
            "value": "Et on le déclare dans notre fichier serveur qu\u2019on a déjà créé :"
        },
        {
            "elementCategory": "code",
            "value": "const server = require('http').createServer(app);\nconst io = require('socket.io')(server);\n"
        },
        {
            "elementCategory": "text",
            "value": "Mais qu\u2019avons-nous fait ? Un serveur dans un serveur ? cela peut paraitre déroutant mais oui, on vient de faire un second serveur. Pourquoi ? Parce que Socket.Io utilise un autre port afin qu\u2019il ne rentre pas en collision avec celui de notre serveur Node. On va donc rajouter cette ligne : "
        },
        {
            "elementCategory": "code",
            "value": "server.listen(3000);"
        },
        {
            "elementCategory": "text",
            "value": "Et comme c\u2019est un peu déroutant, voici le code complet du serveur Node :"
        },
        {
            "elementCategory": "code",
            "value": "const express = require('express');\nconst app = express();\nconst PORT = 8889;\n\nconst server = require('http').createServer(app);\nconst io = require('socket.io')(server);\n\napp.get('/', (req, res) => {\n    res.sendFile(__dirname + '/index.html');\n});\n\nserver.listen(3000);\n\napp.listen(PORT, () => console.log(`Le serveur est sur le http://localhost:${PORT}/`));\n"
        },
        {
            "elementCategory": "text",
            "value": "On a bien avancé, maintenant on va tout simplement capturer dans le serveur le fait qu\u2019un client vient de se connecter via Socket.Io. Voici le code à intégrer dans notre fichier Node :"
        },
        {
            "elementCategory": "code",
            "value": "io.on('connect', (socket) => {\n    console.log('Un client vient de se connecter')\n});\n"
        },
        {
            "elementCategory": "text",
            "value": "C\u2019est bien beau tout ça, mais il faut que le client se connecte au serveur, et plus précisément au port de Socket.Io. En premier lieu, on va ajouter le script de Socket.Io dans le client. Vous pouvez le faire via le module, via un fichier que vous hébergez dans votre ordinateur ou via le CDN. On va opter pour la dernière solution : "
        },
        {
            "elementCategory": "code",
            "value": "&lt;script src=\"https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js\">&lt;/script>"
        },
        {
            "elementCategory": "text",
            "value": "Maintenant que notre client a accès à Socket.Io, on va maintenant lui préciser le numéro de port via cette ligne de code : "
        },
        {
            "elementCategory": "code",
            "value": "const socket = io('ws://localhost:3000');"
        },
        {
            "elementCategory": "text",
            "value": "A ce stade, voici le code complet du client :"
        },
        {
            "elementCategory": "code",
            "value": "&lt;!DOCTYPE html>\n&lt;html lang=\"en\">\n\n&lt;head>\n    &lt;meta charset=\"UTF-8\">\n    &lt;meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n    &lt;title>Tuto Socket.io&lt;/title>\n&lt;/head>\n\n&lt;body>\n    &lt;h1>Salut Socket.io&lt;/h1>\n    &lt;script src=\"https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js\">&lt;/script>\n    &lt;script>\n        const socket = io('ws://localhost:3000');\n    &lt;/script>\n&lt;/body>\n\n&lt;/html>\n"
        },
        {
            "elementCategory": "text",
            "value": "Maintenant, faites autant de rafraichissement que vous voulez, le serveur Node verra qu\u2019un client est présent en affichant le message 'Un client vient de se connecter' sur votre terminal. Bon, on n\u2019est pas encore au temps réel car il faut faire un rafraichissement, mais sachez que grâce à ça, on peut savoir qui est présent et surtout le nombre de client."
        },
        {
            "elementCategory": "subtitle",
            "value": "Et ce message du serveur au client, c\u2019est pour quand ?"
        },
        {
            "elementCategory": "text",
            "value": "Revenons à nos moutons et entrons enfin dans le vif du sujet. Dans le fichiers serveur Node, on remarque qu\u2019on a maintenant une fonction io.on qui porte le nom de « connect ». C\u2019est dans cette même fonction qu\u2019on va envoyer un message au qui sera intercepté par le client sans rechargement."
        },
        {
            "elementCategory": "code",
            "value": "io.on('connect', (socket) => {\n    socket.emit('message', 'Vous êtes bien connecté ! Ce message est juste pour la page qui a fait l\\'appel')\n});\n"
        },
        {
            "elementCategory": "text",
            "value": "Le serveur a la fonction pour envoyer un message, il faut maintenant une fonction coté client pour recevoir ce message."
        },
        {
            "elementCategory": "code",
            "value": "        socket.on('message', function (message) {\n            console.log('Le serveur a un message pour vous : ' + message);\n        })\n"
        },
        {
            "elementCategory": "text",
            "value": "Notez que le premier argument, c\u2019est le mot « message ». Et si vous regardez le code coté serveur, vous voyez aussi l\u2019argument « message » dans la fonction. Pour que le lien entre les deux se fassent, ils doivent avoir le même argument. Je vous donne le code complet."
        },
        {
            "elementCategory": "code",
            "value": "const express = require('express');\nconst app = express();\nconst PORT = 8889;\n\nconst server = require('http').createServer(app);\nconst io = require('socket.io')(server);\n\napp.get('/', (req, res) => {\n    res.sendFile(__dirname + '/index.html');\n});\n\nio.on('connect', (socket) => {\n    console.log('Un client vient de se connecter')\n    socket.emit('message', 'Vous êtes bien connecté ! Ce message est juste pour la page qui a fait l\\'appel')\n});\n\n\nserver.listen(3000);\n\napp.listen(PORT, () => console.log(`Server running on http://localhost:${PORT}/`));\n"
        },
        {
            "elementCategory": "code",
            "value": "&lt;!DOCTYPE html>\n&lt;html lang=\"en\">\n\n&lt;head>\n    &lt;meta charset=\"UTF-8\">\n    &lt;meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n    &lt;title>Tuto Socket.io&lt;/title>\n&lt;/head>\n\n&lt;body>\n    &lt;h1>Salut Socket.io&lt;/h1>\n    &lt;script src=\"https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js\">&lt;/script>\n    &lt;script>\n        const socket = io('ws://localhost:3000');\n\n        socket.on('message', function (message) {\n            console.log('Le serveur a un message pour vous : ' + message);\n        })\n\n    &lt;/script>\n&lt;/body>\n\n&lt;/html>\n"
        },
        {
            "elementCategory": "subtitle",
            "value": "Et quand c\u2019est le client qui dérange le serveur ?"
        },
        {
            "elementCategory": "text",
            "value": "Pour faire la même chose mais dans le sens contraire, ce n\u2019est pas si différent. Coté serveur, au lieu d\u2019utiliser la fonction « emit » (qui envoie), on utilise la fonction « on » (qui écoute)."
        },
        {
            "elementCategory": "code",
            "value": "io.on('connect', (socket) => {\n    socket.on('message3', function (message) { \n        console.log('Un client me parle ! Il me dit : ' + message);\n    })\n});\n"
        },
        {
            "elementCategory": "text",
            "value": "Et c\u2019est coté client qu\u2019on va utiliser la fonction « emit »."
        },
        {
            "elementCategory": "code",
            "value": "socket.emit('message3', 'Salut serveur, ça va ?');"
        },
        {
            "elementCategory": "text",
            "value": "Et voilà ! Ce n\u2019est vraiment pas si sorcier quand on a compris comment fonctionne Socket.Io. Mais tout n\u2019est pas si simple, il y a parfois quelques utilisations un peu plus poussées."
        },
        {
            "elementCategory": "subtitle",
            "value": "Le boadcast"
        },
        {
            "elementCategory": "text",
            "value": "Parfois, on veut envoyer un message au serveur et le serveur doit renvoyer le message aux autres utilisateurs. Sauf que dans le cas de figure qu\u2019on a vu plus haut, le serveur envoie à tous les clients, même à celui qui l\u2019a envoyé en premier. Et franchement, c\u2019est un peu inutile de recevoir notre propre message."
        },
        {
            "elementCategory": "text",
            "value": "Pour que le serveur envoie le message à tous les clients, sauf celui qui a envoyé e message, on va utiliser la fonction broadcast coté serveur."
        },
        {
            "elementCategory": "code",
            "value": "io.on('connect', (socket) => {\n    socket.broadcast.emit(\"message2\", \"ce message est pour toutes les pages, sauf celui qui l'envoi\")\n});\n"
        },
        {
            "elementCategory": "text",
            "value": "Pour le coté client, c\u2019est exactement comme on a vu plus haut, on va juste envoyer le message de client à serveur."
        },
        {
            "elementCategory": "text",
            "value": "Maintenant on va voir un autre cas de figue que vous allez beaucoup utiliser. Je pense même que c\u2019est ce que vous utiliserez le plus avec Socket.Io."
        },
        {
            "elementCategory": "subtitle",
            "value": "Soirée privée"
        },
        {
            "elementCategory": "text",
            "value": "Un chat, c\u2019est deux personnes (ou plus) qui discutent. Et leur discussion ne doit pas être public car seuls les utilisateurs doivent y avoir accès. C\u2019est ici qu\u2019entre en jeu les « room ». Pour schématiser, c\u2019est une pièce privée où seuls les gens possédant une invitation peut y aller et discuter entre eux."
        },
        {
            "elementCategory": "text",
            "value": "D\u2019abord, le client va demander la création d\u2019un « room » auprès du serveur et y adhérer automatiquement."
        },
        {
            "elementCategory": "code",
            "value": "socket.emit('join_room', 'myRoom');"
        },
        {
            "elementCategory": "text",
            "value": "Comme d\u2019habitude, le premier argument est une chaine de charactère qui fait le lien et le second est le nom qu\u2019on envoie. Il faut maintenant que le serveur reçoive l\u2019information."
        },
        {
            "elementCategory": "code",
            "value": "io.on('connect', (socket) => {\n    socket.on(' myRoom ', room => {\n        socket.join(room);\n    });\n});\n"
        },
        {
            "elementCategory": "text",
            "value": "Le serveur vient de créer le salon « myRoom » et il y a mis la personne qui a fait la demande de création. Bien sûr, ne le laisser pas tout seul le pauvre. Pour y rajouter des gens, c\u2019est exactement la même chose. Le serveur saura que le salon existe déjà donc il ne fera pas un doublon. Et bien entendu, il y incorpora toutes les personnes qui y feront la demande. "
        },
        {
            "elementCategory": "text",
            "value": "Si on veut partir du salon, rien de plus simple :"
        },
        {
            "elementCategory": "code",
            "value": "io.on('connect', (socket) => {\n    socket.on('myRoom', room => {\n        socket.leave(room)\n    });\n});\n"
        },
        {
            "elementCategory": "text",
            "value": "Avec la fonction « leave », on peut faire quitter le client qu\u2019on veut."
        },
        {
            "elementCategory": "subtitle",
            "value": "Envoyer un message au salon"
        },
        {
            "elementCategory": "text",
            "value": "Si un client qui fait préalablement parti d\u2019un salon décide d\u2019envoyer un message un message aux autres personnes du salon, voici la marche à suivre. Tout d\u2019abord, voici la fonction coté client :"
        },
        {
            "elementCategory": "code",
            "value": "socket.emit('sendMessage', { room: 'myRoom', message: 'Hello'});"
        },
        {
            "elementCategory": "text",
            "value": "Et coté serveur, on va réceptionner le message. Comme on l\u2019a vu, le premier argument est une chaine de charactère qui va lier la fonction client / serveur."
        },
        {
            "elementCategory": "code",
            "value": "io.on('connect', (socket) => {\n    client.on('sendMessage', function(data) {\n        io.sockets.in(data.room).emit('message', data.message);\n    });\n});\n"
        },
        {
            "elementCategory": "text",
            "value": "La fonction « in » permet de spécifier le nom du salon (qui est en paramètre dans l\u2019objet) et tout de suite après, on envoie le message avec la fonction emit. N\u2019oubliez pas que coté client, on devra faire une fonction « socket.on » qui écoutera le serveur. Mais à ce moment, vous devriez vous en sortir sans problème."
        },
        {
            "elementCategory": "text",
            "value": "A ce stade, vous savez les principales utilisations de Socket.Io. Comme d\u2019habitude, on peut creuser le sujet pour voir toute la force de ce module mais si vous faites comme 90% des utilisateurs, ce petit tuto devrait suffire."
        }
    ],
    "readminutes": 9,
    "createDate": "19/04/2021",
    "__v": 0
}, {
    "_id": {
        "$oid": "5f7b20b929f6cd51f07fc5a5"
    },
    "langage": [
        "react"
    ],
    "category": "Tuto",
    "title": "Le UseContext",
    "description": "Cet article vous explique comment avoir un state global dans votre application react grâce au UseContext",
    "titleLink": "le-usecontext",
    "intro": "La version 16.8 de react a été une petite révolution chez ceux utilisant déjà le langage. En plus de revoir les fondamentaux et de se séparer de certaines choses, la mise à jour a permis de nombreuses améliorations, dont le useContext. Si vous êtes du genre à encapsuler des composants dans des composants et de faire passer des données en escalier, useContext fera votre bonheur.",
    "article": [{
            "elementCategory": "text",
            "value": "Avant de vous expliquer comment utiliser les useContext, voyons un peu la chose. Cette petite méthode, très simple à utiliser, vous permet d\u2019avoir une espèce de « mémoire » interne à votre appli et distribue ces données à qui veut. Vous verrez souvent le terme de « global state ». Oui, c\u2019est un peu comme Redux mais en moins lourd / complexe. Attention, loin de moi l\u2019envie de vous dire que useContext est mieux que Redux ! Disons que UseContext est bien pour des partages de données pas très complexes quand Redux est plus adapté pour des projets avec un global state qui évoluera sans cesse. Bref, ne partez pas toujours du fait que useContext est mieux."
        },
        {
            "elementCategory": "text",
            "value": "Revenons à nos petits moutons. Imaginons que nous avons plusieurs composants et que celui qui est le plus en haut (App) veut passer des données à celui qui est le plus bas (Compo3). Voila comment on ferait sans le useContext."
        },
        {
            "elementCategory": "text",
            "value": "App > Compo1 > Compo2 > Compo3"
        },
        {
            "elementCategory": "code",
            "value": "import React, { useState } from 'react';\nimport Compo1 from \"./Components/Compo1\"\n\nfunction App() {\n\n  const [profile, setProfile] = useState({name:\"Ryan\"})\n\n  return (\n    &lt;Compo1 profile={profile}/>\n  );\n}\n\nexport default App;\n"
        },
        {
            "elementCategory": "code",
            "value": "import React from \"react\"\nimport Compo2 from \"./Compo2\"\n\nexport default function Compo1({profile}){\n\n  return(\n    &lt;Compo2 profile={profile} />\n  )\n}\n"
        },
        {
            "elementCategory": "code",
            "value": "import React from \"react\"\nimport Compo3 from \"./Compo3\"\n\nexport default function Compo2({profile}){\n\n  return(\n    &lt;Compo3 profile={profile} />\n  )\n}\n"
        },
        {
            "elementCategory": "code",
            "value": "import React from \"react\"\n\nexport default function Compo3({profile}){\n\n  return(\n  &lt;div>\n    {profile.name}\n  &lt;/div>\n  )\n}\n"
        },
        {
            "elementCategory": "text",
            "value": "Le composant App a une info qu\u2019il veut faire passer à Compo3, mais il est plusieurs niveaux en dessous, il doit donc le faire passer à tous les composants qui sont entre eux. Non seulement c\u2019est redondant à faire, mais ça peut devenir assez lourd si on veut faire une modification (et oui, il faudra les faire partout). C\u2019est là que le useContext ente en jeu."
        },
        {
            "elementCategory": "text",
            "value": "Cette méthode, née avec les hooks, va nous permettre de passer de la data du composant App au composant Compo3 directement, sans devoir écrire sur les composants qui sont au milieu. Voyons maintenant comment ça se passe."
        },
        {
            "elementCategory": "text",
            "value": "En premier lieu, il va falloir créer la donnée et surtout, le fichier qui le contiendra. Personnellement, je recommande de créer à la racine du projet (ou dans le dossier src comme moi) un dossier qui contiendrai tous les useContext. Voilà à quoi il ressemblera :"
        },
        {
            "elementCategory": "code",
            "value": "import React, { createContext } from 'react'\n\nconst videoGame = createContext()\n\nexport default videoGame\n"
        },
        {
            "elementCategory": "text",
            "value": "Pour ceux qui aiment la clarté, voici comment se présente dorénavant le projet. Pour info, j\u2019ai utilisé le CRA (ceate-react-app) pour avoir rapidement un environnement prêt."
        },
        {
            "elementCategory": "img",
            "value": "01usecontext"
        },
        {
            "elementCategory": "text",
            "value": "Notre variable videoGame est vide, nous avons juste stipulé qu\u2019elle sera un useContext. Et bien sûr, nous l\u2019avons exporté afin qu\u2019elle soit disponible partout dans le projet. Maintenant nous allons l\u2019appelé dans la composant App."
        },
        {
            "elementCategory": "code",
            "value": "import VideoGame from './useContext/videoGame'"
        },
        {
            "elementCategory": "text",
            "value": "L\u2019invoqué c\u2019est bien, l\u2019utilisé c\u2019est mieux. Et c\u2019est là que ça se corse petit peu. Nous avons besoin d\u2019abonné le composant à toutes les futures mises à jour de notre variable videoGame. Nous avons donc besoin d\u2019un provider fourni par React. Voici le composant App au complet."
        },
        {
            "elementCategory": "code",
            "value": "import React, { useState } from 'react';\nimport VideoGame from './useContext/videoGame'\nimport Compo1 from \"./Components/Compo1\"\n\n\nfunction App() {\n\n  const [profile, setProfile] = useState({name:\"Ryan\"})\n\n  return (\n    &lt;VideoGame.Provider>\n      &lt;Compo1/>\n    &lt;/VideoGame.Provider>\n  );\n}\n\nexport default App;\n"
        },
        {
            "elementCategory": "text",
            "value": "Vous avez dû le remarquer, mais j\u2019ai retiré le paramètre « profile » du composant Compo1. Je l\u2019ai aussi fait sur les composants suivants vu qu\u2019on va passer par la méthode useContext cette fois."
        },
        {
            "elementCategory": "text",
            "value": "Souvenez-vous, la variable videoGame est vide, il va donc falloir lui passer une valeur et c\u2019est dans les paramètres du composant Provider que ça va se passer."
        },
        {
            "elementCategory": "code",
            "value": "    &lt;VideoGame.Provider value={\"persona 5\"}>\n      &lt;Compo1/>\n    &lt;/VideoGame.Provider>\n"
        },
        {
            "elementCategory": "text",
            "value": "A partir de maintenant, la valeur qui sera passée au composant sera le nom du jeu. Faisons un peu le point : En premier lieu nous avons une variable vide qui est un useContexte, en second lieu nous avons un composant abonné aux différentes mises à jour de cette variable et enfin, nous donnons à la variable videoGame une valeur."
        },
        {
            "elementCategory": "text",
            "value": "Ce qui manque, c\u2019est de récupérer les données de videoGame dans le compo3 sans passer par les autres.  Et là, vous allez voir que c\u2019est très simple."
        },
        {
            "elementCategory": "code",
            "value": "import React, {useContext} from 'react'\nimport videoGame from \"../useContext/videoGame\"\n\nexport default function Compo3(){\n\n  const gameTitle = useContext(videoGame);\n\n  return(\n  &lt;div>\n    {gameTitle}\n  &lt;/div>\n  )\n}\n"
        },
        {
            "elementCategory": "text",
            "value": "Et voilà le travail ! Il nous suffit simplement d\u2019importer le useContext videoGame et de préciser dans la fonction que c\u2019est un useContext. Nous avons fait ce que nous voulions."
        },
        {
            "elementCategory": "subtitle",
            "value": "Modifier le useContext"
        },
        {
            "elementCategory": "text",
            "value": "On va passer au level au-dessus : Imaginons que Compo3 veut modifier le titre du jeu. Il va donc faire une modification de la variable et comme un utilise un provider pour écouter toutes les modifications, alors la mise à jour se fera automatiquement. J\u2019ai fait quelques modifications sur le composant App mais rien de bien méchant."
        },
        {
            "elementCategory": "code",
            "value": "import React, { useState } from 'react';\nimport VideoGame from './useContext/videoGame'\nimport Compo1 from \"./Components/Compo1\"\n\n\nfunction App() {\n\n  const [gameTitle, setGameTitle] = useState({name:\"Persona 5\"})\n\n  return (\n    &lt;VideoGame.Provider value={{gameTitle, setGameTitle}}>\n      &lt;Compo1/>\n    &lt;/VideoGame.Provider>\n  );\n}\n\nexport default App;\n"
        },
        {
            "elementCategory": "text",
            "value": "Le principal est que dans le paramètre « value » du composant provider VideoGame, on envoie un objet contenant la variable gameTitle (et non plus profile, j\u2019ai changé) et sa fonction afin de ma modifier. Si vous êtes un minimum à l\u2019aise avec les hooks, normalement il ne devrait pas y avoir de compréhension.  Maintenant allons voir le composant Compo3."
        },
        {
            "elementCategory": "code",
            "value": "import React, {useContext} from 'react'\nimport videoGame from \"../useContext/videoGame\"\n\nexport default function Compo3(){\n\n  const { gameTitle, setGameTitle } = useContext(videoGame)\n\n  return(\n  &lt;div>\n    {gameTitle.name}\n    &lt;button onClick={e=>setGameTitle({name:\"Mario\"})}>Changer le nom du jeu&lt;/button>\n  &lt;/div>\n  )\n}\n"
        },
        {
            "elementCategory": "text",
            "value": "Ici, on récupère la variable « gameTitle » et sa fonction modificatrice « setGameTitle » et on les utilise dans le render. Si on clique sur le bouton, vous allez voir que le gameTitle va changer imediatement ! Magique !"
        },
        {
            "elementCategory": "text",
            "value": "Ce petit tuto n\u2019avait pas pour vocation de vous faire devenir un pro du useContext, mais de vous montrer un peu à quoi il peut servir. Mais avant de vous lancer corps et âme, je dois vous dire une chose importante."
        },
        {
            "elementCategory": "subtitle",
            "value": "UseContext / Redux"
        },
        {
            "elementCategory": "text",
            "value": "Soyons clair : le useContext ne remplacera jamais Redux. A premier lieu, on se dit que le useContext peut gérer un state globale comme redux et c\u2019est (presque) vrai. Mais la grande différence est qu\u2019à chaque modification d\u2019une variable useContext, la méthode fait un render des composants, là où Redux ne le fait pas. Cela veut dire que si on a un site assez costaud, il faut éviter de trop utiliser les useContext car tous les composants seront « re-rendu » à chaque modification, et ça risque d\u2019alourdir le site. Quand je dis tous les composants, je parle même de ceux qui ne sont pas encapsulé dans le composant du provider. Il parait qu\u2019il existe un moyen de ne pas avoir ce défaut, et ce moyen se nomme « hookstate ». Prochain article en prévision."
        }
    ],
    "readminutes": 5,
    "createDate": "15/05/2021",
    "__v": 0
}, {
    "_id": {
        "$oid": "5fc5688b3cf33e2ec01cd822"
    },
    "langage": [
        "javascript"
    ],
    "category": "Astuce",
    "Description": "Comment se faire des outils pour que Javascript nous aide dans la vie de tous les jours. Comme par exemple, ne plus avoir de Playstation 5 qui nous passe sous le nez dans les boutiques en ligne",
    "title": "Avoir une Playstation 5 avec Javascript",
    "titleLink": "avoir-une-playstation-5-avec-javascript",
    "intro": "Javascript ne vous sert pas qu\u2019à faire de superbes sites internet ou des serveurs performants, il peut aussi vous servir dans la vie de tous les jours. Comme par exemple, réussir enfin à avoir une Playstation 5. Vous allez me dire que j\u2019ai juste à aller chez mon revendeur préféré pour acheter la grosse bête. Sauf que le jour où j\u2019écris cet article, on est le 19 novembre 2020. Jour de la sortie de la Playstation 5. Et non seulement ce jour a été marqué par une énorme pénurie de la console, mais en plus les principaux gros sites de vente français sont tombés en rade pendant plusieurs heures. Pourquoi ? Parce que tout le monde voulait une PS5 et il n\u2019y en avait pas pour tout le monde. Je ne vous raconte pas l\u2019explosion de connexion\u2026 ",
    "article": [{
            "elementCategory": "subtitle",
            "value": "C\u2019est quoi le rapport avec Javascript ?"
        },
        {
            "elementCategory": "text",
            "value": "Pourquoi faire F5 sur la page de vente de la console toutes les 10 secondes quand un script peut le faire à votre place ? Pourquoi tout le temps regarder si le bouton « ajouter au panier » est présent quand un script peut vous prévenir de son apparition ? Je pense que vous avez compris où je veux en venir."
        },
        {
            "elementCategory": "text",
            "value": "Petite précision, cet article n\u2019est pas vraiment là pour vous dire comment choper une Playstation 5 sous le nez d\u2019autres acheteurs, mais pour vous montrer que faire du code peut vous aider de bien des manières. Par exemple, j\u2019ai personnellement fait un script JS pour me dire qu\u2019elles sont les meilleurs objets pour faire le maximum de dégâts dans le jeu vidéo League of Legends, en tenant compte de mon niveau et de l\u2019or acquis."
        },
        {
            "elementCategory": "subtitle",
            "value": "Injection"
        },
        {
            "elementCategory": "text",
            "value": "La première étape, c\u2019est d\u2019injecter un code dans la page de notre chère petite Playstation. Pas de panique, je ne parle pas de faire des injections sur le serveur comme tout bon pirate. Ici, on ne touche qu\u2019au code du front, donc du navigateur. Pour ça, nous avons besoin d\u2019un module qui se nomme « Code injector » et qui est disponible pour le navigateur Firefox. Vous ne devriez pas avoir trop de mal à le trouver ni à l\u2019installer."
        },
        {
            "elementCategory": "text",
            "value": "Une fois que vous avez lancé le module, vous devriez avoir ce genre de présentation."
        },
        {
            "elementCategory": "img",
            "value": "01"
        },
        {
            "elementCategory": "text",
            "value": "Vous l\u2019avez certainement deviné mais ce module nous permet d\u2019injecter du code dans le navigateur afin qu\u2019il l\u2019applique dans la page visée."
        },
        {
            "elementCategory": "subtitle",
            "value": "Code"
        },
        {
            "elementCategory": "text",
            "value": "Le principe est simple : On a dire à notre navigateur de voir si la Playstation 5 est dispo. Si c\u2019est le cas, il va jouer une musique. Si ce n\u2019est pas le cas, il attend 5 minutes et faire un rechargement de la page. Et il recommence la boucle. Ça à l\u2019air simple, mais ce n\u2019est pas tout à fait le cas. La vraie difficulté réside dans le fait que le code doit analyser la page et voir si une Playstation 5 est dispo. Et pour ça, on va devoir se débrouiller avec le HTML envoyé par le site serveur du site. Prenons par exemple le cas de la boutique en ligne Micromania."
        },
        {
            "elementCategory": "text",
            "value": "Juste à l\u2019œil, on voit la différence quand un produit est disponible ou pas. Et cette différence, c\u2019est la présence du bouton « Ajouter au panier ». Ce bouton, il a une classe spécifique qui se nomme « btn-add-basket ». Si cette classe n\u2019est pas présente dans le HTML, alors ça veut dire que la console n\u2019est pas disponible. Si elle est présente, alors la console est là. Donc nous allons injecter ce code :"
        },
        {
            "elementCategory": "code",
            "value": "setTimeout(() => {\n\n  const elem = document.getElementsByClassName('btn-add-basket')\n  console.log(\"elem\", elem)\n\n  if (elem.length > 0) {\n    console.log('PS5 DISPO !')\n    const audio = new Audio('http://commondatastorage.googleapis.com/codeskulptor-demos/DDR_assets/Kangaroo_MusiQue_-_The_Neverwritten_Role_Playing_Game.mp3');\n    audio.play();\n  } else {\n    console.log('Pas de ps5')\n    setTimeout(() => {\n      document.location.reload()\n    }, 60000);\n  }\n\n}, 3000);\n"
        },
        {
            "elementCategory": "subtitle",
            "value": "j\u2019ai une ps5 !"
        },
        {
            "elementCategory": "text",
            "value": "Ce code est très simple et une personne qui a les bases en Javascript n\u2019aura aucun mal à le comprendre. Il suffit de l\u2019injecter avec notre module et attendre qu\u2019une musique se lance. En réalité, tout n\u2019est pas si rose. Je ne sais pas si ça vient du navigateur ou du module, mais il y a une fuite de mémoire qui cause un plantage de Firefox au bout de plusieurs heures. Personnellement, ça ne m\u2019a pas posé de problème car les pages et l\u2019injection de code reviennent automatiquement après que Firefox ait planté."
        },
        {
            "elementCategory": "text",
            "value": "C\u2019est beau non ? Encore une fois, je précise que ce n\u2019est pas un tuto pour avoir une playstation 5, mais plutôt un article pour vous montrer qu\u2019on peut utiliser du code Javascript pour plein de choses. Le monde du code est si vaste\u2026"
        }
    ],
    "readminutes": 4,
    "createDate": "05/25/2021",
    "__v": 0
}, {
    "_id": {
        "$oid": "5fc5688b3cf33e2ec01cd855"
    },
    "langage": ["react"],
    "category": "Astuce",
    "Description":"Beaucoup de nouvelles méthodes ont vu le jour depuis l'apparition des Hooks en React. L'une d'elles se nomme HookState et même si elle n'est pas une feature officielle de React, elle a de sérieux atouts face au useContext",
    "title": "Hookstate",
    "titleLink": "hookstate",
    "intro": "Il y a quelques semaines, j’avais fait un article sur le Usecontext. Outil très intéressant si notre projet ne requiert pas Redux ou tout autre state global complexe. Mais son défaut était qu’à chaque évolution de la state globale, tous les composants étaient re-rendu. Pas très grave pour un petit projet, mais quand il devient un minimum conséquent, les performances peuvent être impacter. C’est pour cette raison qu’aujourd’hui, nous allons voir ensemble un outil révolutionnaire mais tristement méconnu : le hookState.",
    "article": [{
        "elementCategory": "subtitle",
        "value": "Le changement c’est maintenant"
    }, {
        "elementCategory": "text",
        "value": "Hookstate n’est pas très connu et si je vous en parle maintenant, c’est surtout parce que je suis tombé par hasard sur un article qui vantait ses mérites. L’avantage qu’il a face au UseContext, c’est qu’il ne force pas un nouveau rendu de tous les composants, seulement ceux qui en ont vraiment besoin. Et par rapport à Redux, c’est ses performances et sa simplicité. Voici l’article en question."
    }, {
        "elementCategory": "link",
        "value": "https://praisethemoon.org/hookstate-how-one-small-react-library-saved-moonpiano/"
    }, {
        "elementCategory": "text",
        "value": "Maintenant que vous êtes presque convaincu de la puissance du Hookstate, mettons-nous tout de suite dans le code en expliquant pas à pas."
    }, {
        "elementCategory": "subtitle",
        "value": "Install"
    }, {
        "elementCategory": "text",
        "value": "Avant de rentrer dans le vif du sujet, je tiens à préciser que pour cet exemple, j’utilise le CRA (create-react-app), très pratique pour la présentation des exemples. Bref, on va passer par le npm pour installer notre petit module. Je parle de npm mais bien entendu, les utilisateurs de yarn peuvent aussi l’installer."
    }, {
        "elementCategory": "code",
        "value": "npm i @hookstate/core"
    }, {
        "elementCategory": "text",
        "value": "Maintenant que notre module est installé, nous allons en premier lieu créer notre Global State. Et pour ça, rien de plus simple : un simple fichier dans le projet. Personnellement, je nomme ce fichier index.js et je le mets  dans un dossier « globalState » pour plus de lisibilité. Voici ce que notre nouveau fichier contiendra."
    }, {
        "elementCategory": "code",
        "value": "import React from 'react';\nimport { createState } from '@hookstate/core';\n\nconst globalState = createState(0);\n\nexport default globalState\n"
    }, {
        "elementCategory": "text",
        "value": "Rien de complexe ici : On voit qu’on a créé une variable globalState qui est un createState qui a un 0. Pour faire court, notre globalState équivaut à zero."
    }, {
        "elementCategory": "subtitle",
        "value": "App"
    }, {
        "elementCategory": "text",
        "value": "Maintenant que notre globalState est initialisé, nous allons nous intéresser aux composants. En premier lieu, on va aller dans le fichier « App.js » présent dans le dossier « src » pour y incorporer un composant que nous appellerons « Compo1 ». Voici donc notre fichier « App.js »."
    }, {
        "elementCategory": "code",
        "value": "import React from 'react';\n\nimport Compo1 from \"./composants/compo1\"\n\nfunction App() {\n  return (\n    <div>\n      <Compo1/>\n    </div>\n  );\n}\n\nexport default App;\n"
    }, {
        "elementCategory": "text",
        "value": "Et dans le composant « Compo1 » que nous allons créer dans le dossier « composants », nous allons simplement initialisé le composant."
    }, {
        "elementCategory": "code",
        "value": "import React from 'react';\n\nexport default function Compo1(){\n\n  return <>\n      <p>Hello Compo1</p>\n  </>\n}\n"
    }, {
        "elementCategory": "text",
        "value": "Maintenant que tout est prêt, on va faire appel au state global que nous avons préparé il y a quelques minutes. Pour ça, on va devoir invoquer le useState et le globalState et l’afficher dans le HTML."
    }, {
        "elementCategory": "code",
        "value": "import React from 'react';\nimport { useState } from '@hookstate/core';\nimport globalState from \"../globalState/index\"\n\nexport default function Compo1(){\n\n  const state = useState(globalState);\n  return <>\n      {console.log(state.get())}\n  </>\n}\n"
    }, {
        "elementCategory": "text",
        "value": "Comme vous le voyez, on utilise le « useState » pour mettre notre « globalState » dans une nouvelle variable qui se nommera tout simplement « state ». Pour l’afficher dans le HTML, il ne faut pas juste mettre « {state} » mais « {state.get()} ». Sinon vous aurez une belle erreur. Normalement ça fonctionne et vous voyez dans votre navigateur la valeur de notre « globalState »."
    }, {
        "elementCategory": "text",
        "value": "On sait comment afficher, maintenant on va voir comment faire évoluer. On va simplement faire une buton qui changera ce state, toujours dans le composant 1."
    }, {
        "elementCategory": "code",
        "value": "import React, { useEffect } from 'react';\nimport { useState } from '@hookstate/core';\nimport globalState from \"../globalState/index\"\n\n\nexport default function Compo1(){\n \n  const state = useState(globalState);\n  return <>\n      {state.get()}\n      <button onClick={()=> state.set(1)}>Evolution</button>\n  </>\n}\n"
    }, {
        "elementCategory": "text",
        "value": "Voilà un petit code pas très compliqué à comprendre. Sachez que lorsque nous cliquons sur le buton, notre « state » deviens 1 au lieu de 0. Et il est affiché en temps réel directement dans notre composant. "
    }, {
        "elementCategory": "subtitle",
        "value": "Compo2"
    }, {
        "elementCategory": "text",
        "value": "On va maintenant faire un nouveau composant que nous nommerons « Compo2 ». Et dans ce composant, on va afficher ce fameux globalState."
    }, {
        "elementCategory": "code",
        "value": "import React from \"react\"\nimport { useState } from '@hookstate/core';\nimport globalState from \"../globalState/index\"\n\nexport default function Compo2(){\n\n  const state = useState(globalState);\n\n  return <>\n      {state.get()}\n    </>\n}\n"
    }, {
        "elementCategory": "text",
        "value": "Bien entendu, n’oubliez pas d’invoquez votre nouveau composant dans le fichier « App.js »"
    }, {
        "elementCategory": "code",
        "value": "import React from 'react';\n\nimport Compo1 from \"./composants/compo1\"\nimport Compo2 from \"./composants/compo2\"\n\n\nfunction App() {\n  return (\n    <div>\n      <Compo1/>\n     <Compo2/>\n    </div>\n  );\n}\n\nexport default App;\n"
    }, {
        "elementCategory": "text",
        "value": "Ce que le code fera maintenant, c’est d’afficher en temps réel notre « globalState » dans les deux composants, même lorsqu’il changera en cliquant sur le bouton. Le tout nous rappelle le « useContext » et c’est normal. D’ailleurs on ne comprend pas encore pourquoi on utiliserait le « hookState » au lieu du « useContext ». C’est justement ce qu’on va voir."
    }, {
        "elementCategory": "subtitle",
        "value": "Le changement c’est toujours pas maintenant"
    }, {
        "elementCategory": "text",
        "value": "On va maintenant faire un troisième composant que l’on va appeler « Compo3 ». Vous connaissez la marche à suivre : On créer le composant dans le dossier « Composant » avec les autres et on n’oublie pas de l’appeler dans le fichier « App.js »."
    }, {
        "elementCategory": "text",
        "value": "Dans ce troisième composant, on ne va presque rien faire, simplement mettre un « console.log » dans le rendu HTML."
    }, {
        "elementCategory": "code",
        "value": "import React from \"react\"\n\nexport default function Compo3(){\n\n  return <>\n      {console.log('Je suis le composant 3')}\n    </>\n}\n"
    }, {
        "elementCategory": "text",
        "value": "A quoi va servir ce troisième composant ? C’est simple, il va nous servir à voir dans la console du navigateur s’il a été re-rendu ou pas. Si vous avez lu mon article sur le « useContext », on a vu que lorsque le state globale change, tous les composants était re-rendu, même les composants qui ne sont pas encapsuler dans le « Provider ». En termes de performance, c’est pas top."
    }, {
        "elementCategory": "text",
        "value": "Revenons à notre projet et cliquons sur le bouton pour modifier notre state globale comme on l’a fait auparavant. « Compo1 » et « Compo2 » ont eu un re-rendu car tous les deux doivent afficher la state global comme prévu. Mais la bonne nouvelle, c’est que le troisième composant n’a pas eu de re-rendu. Et c’est justement ça la force de « HookState » ! Vous voulez une preuve ? Cliquez sur le bouton et regardez dans la console de votre navigateur. Vous verrez que le message « Je suis le composant 3 » ne s’affiche pas. Il ne s’affiche pas car le composant n’a pas eu recours au re-rendu."
    }, {
        "elementCategory": "text",
        "value": "Et voilà, maintenant je vous ai montré la grande force du « hookState ». Je ne dis pas que c’est mieux que le « useContext » car tout dépend de vos projets mais pour un projet avec plusieurs composants, il faudra y penser fortement."
    }],
    "readminutes": 7,
    "createDate": "02/07/2021",
}]

export default articlesJson