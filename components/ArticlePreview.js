import Tags from './micro/tags'
import '../styles/articlePreview.scss'

export default({articlePreview}) => {
    return (
        <div className="articlePreviewFrame">
            <div className="articlePreview">
                <div className="columns">
                    <div className="column">
                        <figure className="image">
                            <a href={`/article/${articlePreview.titleLink}`}>
                                {/* eslint-disable-next-line no-undef */}
                                <img src={`/articlepreview/${articlePreview.title}.png`} alt={articlePreview.title}/>
                            </a>
                        </figure>
                    </div>
                </div>
                <div className="columns">
                    <div className="column is-11 is-offset-1">
                        <Tags tags={articlePreview.langage} size={'normal'}/>
                    </div>
                </div>
                <div className="columns titleFrame is-vcentered">
                    <div className="column is-10 is-offset-1">
                        <a href={`/article/${articlePreview.titleLink}`}>

                        <p className='title is-6'>{articlePreview.title}</p>
                        </a>
                    </div>
                </div>
                <div className="columns dateFrame mb-1">
                    <div className="column is-6">{articlePreview.createDate}</div>
                    <div className="column is-6 has-text-right">{articlePreview.readminutes} min de lecture</div>
                </div>
            </div>
        </div>
    )
}