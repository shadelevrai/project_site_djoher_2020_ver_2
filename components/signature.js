import {IoIosMail} from 'react-icons/io';
import {FaLinkedin} from 'react-icons/fa'

import '../styles/signature.scss'

export default({articleDateCreate, articleReadminutes}) => {
    return (
        <div className="columns signature is-vcentered ">
            <div className="column is-offset-4-mobile">
                <figure className="image is-128x128">
                    <img src="/me_circle.jpg" className="is-rounded"/>
                </figure>
            </div>
            <div className="column is-offset-4-mobile is-4-mobile is-6-desktop">
                {/* eslint-disable-next-line no-undef */}
                <p className="has-text-weight-semibold is-size-5">{process.env.nameLastName}</p>
                <p>{articleDateCreate}&nbsp;-&nbsp;{articleReadminutes}&nbsp;minutes de lecture
                </p>
            </div>
            <div className="column is-offset-4-mobile is-5-mobile is-3-desktop">
                {/* eslint-disable-next-line no-undef */}
                <a href={`mailto:${process.env.mailPro}`}>
                    <IoIosMail className='is-large icon has-text-dark'/>
                </a>
                <a href={process.env.linkedinWebPage}>
                    <FaLinkedin className='is-large icon has-text-dark'/>
                </a>
            </div>
        </div>
    )
}