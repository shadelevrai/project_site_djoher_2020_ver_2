import ArticlePreview from './ArticlePreview'
import '../styles/articleList.scss'

export default({
    articlesJson,
    options = {
        search: false
    }
}) => {
    console.log("articlesJson", articlesJson)
    return (
        <article>
            <div className="columns is-multiline articleList is-centered">
                {options.search ? articlesJson
                    .filter(data=>data.langage.find(element=>element === options.search))
                    .map(item => <div key={item.title} className="column">
                        <ArticlePreview articlePreview={item}/>
                    </div>)
                    :
                    articlesJson
                    .map(item => <div
                        key={item.title}
                        className="column is-narrow">
                        <ArticlePreview articlePreview={item}/>
                    </div>)}
            </div>
        </article>
    )
}