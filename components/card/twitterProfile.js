import { useEffect } from "react"
import "../../styles/twitterProfile.scss"

export default({profile})=>{

  return (
    <div className="columns is-gapless twitterProfile mb-3">
      <div className="column is-4">
      <figure className="image is-64x64">
        <img className="is-rounded" src={profile.imgLink}/>
      </figure>
      </div>
      <div className="column is-7 ml-2">
          <div className="is-size-7 has-text-weight-bold">{profile.name}</div>
          <div className="is-size-7">{profile.description}</div>
          <div className="has-background-info twitterButton mt-1">
            <a href={profile.followLink} className="is-size-7 has-text-white" target="_blank">
              <p className="has-text-centered is-size-7">{profile.followName}</p>
            </a>
          </div>
      </div>
    </div>
  )
}