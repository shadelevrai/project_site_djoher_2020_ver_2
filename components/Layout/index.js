import Header from './header'
import Footer from './footer'

import '../../styles/layoutIndex.scss'

export default ({children}) => {
    const {langage} = children.props
    return (
        <>
            <Header langage={langage}/>
            <div className="columns is-centered" id="layoutIndex">
                <div className="column is-narrow">
                    <div className="space">
                    {children}
                    </div>
                </div>
            </div>
            <Footer langage={langage}/>
        </>
    )
}