import '../../styles/footer.scss'

export default({langage}) => {
    return (
        <footer className="footerFrame pt-5 pb-4">
            <div className="columns">
                <div className="column is-offset-1-mobile is-10-mobile is-3 is-offset-1">
                    <p className="is-uppercase has-text-white has-text-weight-bold">Guide</p>
                    {langage.data.map(item=> 
                    <a key={item.name} className=" has-text-white" href={`/guide/${item.link}`}>
                        <span>{item.name} / </span>
                    </a>
                    )}
                </div>
                <div className="column is-offset-1-mobile is-10-mobile is-3 is-offset-1">
                    <p className="is-uppercase has-text-white has-text-weight-bold" >Contact</p>
                    <a href={`mailto:${process.env.mailPro}`}>

                    <p className="has-text-white">{process.env.mailPro}</p>
                    </a>
                    <a href={process.env.linkedinWebPage}>
                    <p className="has-text-white">Linkedin</p>
                    </a>
                </div>
                <div className="column is-offset-1 is-1 mt-0 is-offset-3-mobile is-6-mobile">
                  <figure className="image">
                    <img src="/qrcode.png"/>
                  </figure>
                </div>
            </div>
            <div className="has-text-centered has-text-white">{process.env.nameWebSite} par {process.env.nameLastName}</div>
        </footer>
    )
}