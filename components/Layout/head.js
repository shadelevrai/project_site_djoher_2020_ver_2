import Head from 'next/head'

export default () => {
    return <>
    <Head>
      <title>Binevenue sur Djoher-dev</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link rel="shortcut icon" href="/favicon.png" />
    </Head>
  </>
    
}