import '../../styles/header.scss'

export default({langage}) => {

    

    function setBurgerBar() {
        const burger = document.querySelector('.burger');
        const nav = document.querySelector(`#${burger.dataset.target}`);
        burger
            .classList
            .toggle('is-active');
        nav
            .classList
            .toggle('is-active');
    }

    return (
            <header className={`headers header`}>
                <div className="columns">
                    <div className="column is-4">
                        <a href="/">
                            <div className="level">
                                <div className="level-item">
                                    <div className="circleRed has-background-success"></div>
                                    <p className='djoher'>{'{DJOHER}'}
                                        <span className='isdev'>&nbsp; dev</span>
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div className="column is-8">
                        <nav className="navbar" role="navigation" aria-label="main navigation">
                            <div className="navbar-brand">
                                <a
                                    role="button"
                                    className="navbar-burger burger"
                                    aria-label="menu"
                                    aria-expanded="false"
                                    data-target="navbarBasicExample"
                                    onClick={() => {
                                    setBurgerBar()
                                }}
                                    href='/#'>
                                    <span aria-hidden="true"></span>
                                    <span aria-hidden="true"></span>
                                    <span aria-hidden="true"></span>
                                </a>
                            </div>
                            <div id="navbarBasicExample" className="navbar-menu mr-6">
                                <div className="navbar-end">
                                    <div className="navbar-item has-dropdown is-hoverable">
                                        <a className="navbar-link" href='/guide'>
                                            Guide
                                        </a>
                                        <div className="navbar-dropdown">
                                            {langage
                                                .data
                                                .map((item) => <a key={item.name} className="navbar-item" href={`/guide/${item.link}`}>{item.name}</a>)}
                                        </div>
                                    </div>
                                    <a href="/exercices" className="navbar-item">
                                        Exercices
                                    </a>
                                    <a href="/me" className="navbar-item">
                                        A propos de moi
                                    </a>
                                    <a href="/link" className="navbar-item">
                                        Liens utiles
                                    </a>
                                    <a href="/contact" className="navbar-item">
                                        <p>Me contacter</p>
                                    </a>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </header>
    )
}