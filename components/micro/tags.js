import '../../styles/tags.scss'

export default({tags, size}) => {
    return (
        <div>
            {tags.map(tag => <span key={tag} className={`langageFrame tag is-success is-light is-${size}`}>
                <a href={`/guide/${tag}`}>
                    <span>{tag}</span>
                </a>
            </span>)}
        </div>
    )
}